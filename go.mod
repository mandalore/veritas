module gitlab.com/mandalore/veritas

require (
	github.com/aws/aws-sdk-go v1.30.9
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/mock v1.4.3
	github.com/hashicorp/golang-lru v0.5.4
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/linkedin/goavro/v2 v2.9.7
	github.com/onsi/gomega v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/qri-io/jsonschema v0.1.1
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.5.1
	github.com/tidwall/gjson v1.6.0
	golang.org/x/sys v0.0.0-20190826190057-c7b8b68b1456 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)

go 1.16
