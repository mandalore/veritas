package veritas

import (
	"context"
	"fmt"
)

// Collection type holding a Collection handler
type Collection struct {
	slug    string
	blocks  BlockStore
	schemas SchemaStore
}

func newCollection(slug string, blocks BlockStore, schemas SchemaStore) (Collection, error) {
	Collection := Collection{
		slug:    slug,
		blocks:  blocks,
		schemas: schemas,
	}

	return Collection, nil
}

// Register stores a new Schema for this Collection
func (c Collection) Register(ctx context.Context, section string, schema Schema) error {
	if err := c.schemas.Store(ctx, c.slug, section, schema); err != nil {
		return fmt.Errorf("failed to register schema for collection %s: %w", c.slug, err)
	}

	return nil
}

func (c Collection) DocumentFor(ctx context.Context, dID string) (Document, error) {
	var (
		doc  = Document{Collection: c.slug, ID: dID}
		pMap = map[string]int{}
	)

	blks, err := c.blocks.BlocksForDocument(ctx, c.slug, dID)
	if err != nil {
		return Document{}, fmt.Errorf("failed to fetch document with key %s for collection %s: %w", dID, c.slug, err)
	}

	for _, blk := range blks {
		pos, ok := pMap[blk.Section]
		if !ok {
			doc.Sections = append(doc.Sections, Section{Slug: blk.Section})
			pos = len(doc.Sections) - 1
			pMap[blk.Section] = pos
		}

		doc.Sections[pos].Blocks = append(doc.Sections[pos].Blocks, Block{ID: blk.ID, Version: blk.Version, Payload: blk.Payload, Facts: blk.Facts})
	}

	return doc, nil
}

func (c Collection) SectionsFor(ctx context.Context, dID string, sections ...string) ([]Section, error) {
	var (
		pMap = map[string]int{}
		res  = []Section{}
	)

	blks, err := c.blocks.BlocksForSections(ctx, c.slug, dID, sections)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch document with key %s for collection %s: %w", dID, c.slug, err)
	}

	for _, blk := range blks {
		pos, ok := pMap[blk.Section]
		if !ok {
			res = append(res, Section{Slug: blk.Section})
			pos = len(res) - 1
			pMap[blk.Section] = pos
		}

		res[pos].Blocks = append(res[pos].Blocks, Block{ID: blk.ID, Version: blk.Version, Payload: blk.Payload, Facts: blk.Facts})
	}

	return res, nil
}

func (c Collection) BlocksInSection(ctx context.Context, dID string, section string, bIDs ...string) ([]Block, error) {
	var res = []Block{}

	blks, err := c.blocks.BlocksInSection(ctx, c.slug, dID, section, bIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch document with key %s for collection %s: %w", dID, c.slug, err)
	}

	for _, blk := range blks {
		res = append(res, Block{ID: blk.ID, Version: blk.Version, Payload: blk.Payload, Facts: blk.Facts})
	}

	return res, nil
}

// Store stores a Document on the collection
func (c Collection) StoreDocumentSections(ctx context.Context, dID string, sections ...Section) error {
	var recs = []BlockRecord{}

	for _, s := range sections {
		schema, err := c.schemas.SchemaFor(ctx, c.slug, s.Slug)
		if err != nil {
			return fmt.Errorf("failed to fetch schema for collection %s: %w", c.slug, err)
		}

		for _, b := range s.Blocks {
			if err := schema.IsValid(); err != nil {
				return err
			}

			if err := schema.Validate(b); err != nil {
				return fmt.Errorf("block with key %s:%s is not compliant with schema for collection %s: %w", s.Slug, b.ID, c.slug, err)
			}

			rec, err := NewBlockRecord(schema, c.slug, dID, s.Slug, b)
			if err != nil {
				return err
			}

			recs = append(recs, rec)
		}

	}

	if err := c.blocks.StoreBlockRecords(ctx, recs...); err != nil {
		return fmt.Errorf("failed to store document block with key %s in collection %s: %w", dID, c.slug, err)
	}

	return nil
}
