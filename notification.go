package veritas

// Notification ...
type Notification struct {
	Key     interface{}
	Payload interface{}
	Err     error
}
