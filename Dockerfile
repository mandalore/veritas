FROM golang:1.12-alpine

RUN apk add protobuf git make
RUN go get github.com/vektra/mockery/.../ github.com/golang/protobuf/proto github.com/golang/protobuf/protoc-gen-go

COPY . /build/
WORKDIR /build/

RUN make install
