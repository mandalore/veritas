package veritas

import (
	"context"
	"errors"
	"fmt"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestDocumentFor(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)

	defer ctrl.Finish()

	testCases := []struct {
		description string
		collection  string
		schemaStore func() *MockSchemaStore
		blockStore  func() *MockBlockStore
		expected    Document
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when block store returns an error",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksForDocument(ctx, "sample", "documentID").Return(nil, errors.New("boom"))

				return mock
			},
			err: assert.Error,
		},
		{
			description: "when block store returns an empty set of blocks",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksForDocument(ctx, "sample", "documentID").Return(nil, nil)

				return mock
			},
			expected: Document{ID: "documentID", Collection: "sample"},
			err:      assert.NoError,
		},
		{
			description: "when block store returns a set of blocks",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksForDocument(ctx, "sample", "documentID").Return([]BlockRecord{
					{Collection: "sample", DocumentID: "documentID", Section: "section1", ID: "block1", Version: 1, Payload: []byte(`{"block1":true}`)},
					{Collection: "sample", DocumentID: "documentID", Section: "section2", ID: "block2", Version: 1, Payload: []byte(`{"block1":true}`)},
					{Collection: "sample", DocumentID: "documentID", Section: "section1", ID: "block3", Version: 1, Payload: []byte(`{"block1":true}`)},
					{Collection: "sample", DocumentID: "documentID", Section: "section3", ID: "block4", Version: 1, Payload: []byte(`{"block1":true}`)},
				}, nil)

				return mock
			},
			expected: Document{
				ID:         "documentID",
				Collection: "sample",
				Sections: []Section{
					{
						Slug: "section1",
						Blocks: []Block{
							{ID: "block1", Version: 1, Payload: []byte(`{"block1":true}`)},
							{ID: "block3", Version: 1, Payload: []byte(`{"block1":true}`)},
						},
					},
					{
						Slug: "section2",
						Blocks: []Block{
							{ID: "block2", Version: 1, Payload: []byte(`{"block1":true}`)},
						},
					},
					{
						Slug: "section3",
						Blocks: []Block{
							{ID: "block4", Version: 1, Payload: []byte(`{"block1":true}`)},
						},
					},
				},
			},
			err: assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			collection, err := newCollection(tc.collection, tc.blockStore(), tc.schemaStore())
			if err != nil {
				t.Error("failed with", err)
			}

			doc, err := collection.DocumentFor(ctx, "documentID")

			tc.err(t, err, "should assert the error expectation")
			assert.Equal(t, tc.expected, doc, "should have returned the expected response")
		})
	}
}

func TestSectionFor(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)

	defer ctrl.Finish()

	testCases := []struct {
		description string
		collection  string
		schemaStore func() *MockSchemaStore
		blockStore  func() *MockBlockStore
		expected    []Section
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when block store returns an error",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksForSections(ctx, "sample", "documentID", []string{"section1", "section2"}).Return(nil, errors.New("boom"))

				return mock
			},
			err: assert.Error,
		},
		{
			description: "when block store returns an empty set of blocks",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksForSections(ctx, "sample", "documentID", []string{"section1", "section2"}).Return(nil, nil)

				return mock
			},
			expected: []Section{},
			err:      assert.NoError,
		},
		{
			description: "when block store returns a set of blocks",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksForSections(ctx, "sample", "documentID", []string{"section1", "section2"}).Return([]BlockRecord{
					{Collection: "sample", DocumentID: "documentID", Section: "section1", ID: "block1", Version: 1, Payload: []byte(`{"block1":true}`)},
					{Collection: "sample", DocumentID: "documentID", Section: "section2", ID: "block2", Version: 1, Payload: []byte(`{"block1":true}`)},
					{Collection: "sample", DocumentID: "documentID", Section: "section1", ID: "block3", Version: 1, Payload: []byte(`{"block1":true}`)},
				}, nil)

				return mock
			},
			expected: []Section{
				{
					Slug: "section1",
					Blocks: []Block{
						{ID: "block1", Version: 1, Payload: []byte(`{"block1":true}`)},
						{ID: "block3", Version: 1, Payload: []byte(`{"block1":true}`)},
					},
				},
				{
					Slug: "section2",
					Blocks: []Block{
						{ID: "block2", Version: 1, Payload: []byte(`{"block1":true}`)},
					},
				},
			},
			err: assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			collection, err := newCollection(tc.collection, tc.blockStore(), tc.schemaStore())
			if err != nil {
				t.Error("failed with", err)
			}

			doc, err := collection.SectionsFor(ctx, "documentID", "section1", "section2")

			tc.err(t, err, "should assert the error expectation")
			assert.Equal(t, tc.expected, doc, "should have returned the expected response")
		})
	}
}

func TestBlocksInSection(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)

	defer ctrl.Finish()

	testCases := []struct {
		description string
		collection  string
		schemaStore func() *MockSchemaStore
		blockStore  func() *MockBlockStore
		expected    []Block
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when block store returns an error",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksInSection(ctx, "sample", "documentID", "section1", []string{"block1", "block2"}).Return(nil, errors.New("boom"))

				return mock
			},
			err: assert.Error,
		},
		{
			description: "when block store returns an empty set of blocks",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksInSection(ctx, "sample", "documentID", "section1", []string{"block1", "block2"}).Return(nil, nil)

				return mock
			},
			expected: []Block{},
			err:      assert.NoError,
		},

		{
			description: "when block store returns a set of blocks",
			collection:  "sample",
			schemaStore: func() *MockSchemaStore { return NewMockSchemaStore(ctrl) },
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().BlocksInSection(ctx, "sample", "documentID", "section1", []string{"block1", "block2"}).Return([]BlockRecord{
					{Collection: "sample", DocumentID: "documentID", Section: "section1", ID: "block1", Version: 1, Payload: []byte(`{"block1":true}`)},
					{Collection: "sample", DocumentID: "documentID", Section: "section1", ID: "block2", Version: 1, Payload: []byte(`{"block1":true}`)},
				}, nil)

				return mock
			},
			expected: []Block{
				{ID: "block1", Version: 1, Payload: []byte(`{"block1":true}`)},
				{ID: "block2", Version: 1, Payload: []byte(`{"block1":true}`)},
			},
			err: assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			collection, err := newCollection(tc.collection, tc.blockStore(), tc.schemaStore())
			if err != nil {
				t.Error("failed with", err)
			}

			doc, err := collection.BlocksInSection(ctx, "documentID", "section1", "block1", "block2")

			tc.err(t, err, "should assert the error expectation")
			assert.Equal(t, tc.expected, doc, "should have returned the expected response")
		})
	}
}

func TestStore(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)

		eBlk = Block{
			ID:      "12",
			Version: 11,
			Payload: []byte(`{"references":[{"code":12},{"code":13]}`),
		}

		koValidator, _ = NewValidator("denyall", nil)

		dig = []byte{96, 94, 75, 71, 188, 185, 119, 183, 107, 111, 109, 226, 191, 182, 5, 57, 222, 170, 222, 72, 136, 80, 130, 66, 231, 222, 15, 178, 105, 95, 99, 211}

		okValidator, _ = NewValidator("acceptall", nil)
	)

	defer ctrl.Finish()

	testCases := []struct {
		description string
		collection  string
		schemaStore func() *MockSchemaStore
		blockStore  func() *MockBlockStore
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when schema repository returns an error",
			collection:  "zdocs",
			schemaStore: func() *MockSchemaStore {
				mock := NewMockSchemaStore(ctrl)

				mock.EXPECT().SchemaFor(ctx, "zdocs", "tst").Return(Schema{}, fmt.Errorf("failed"))

				return mock
			},
			blockStore: func() *MockBlockStore { return NewMockBlockStore(ctrl) },
			err:        assert.Error,
		},
		{
			description: "when schema repository returns a schema but doc does not have correct schema",
			collection:  "zdocs",
			schemaStore: func() *MockSchemaStore {
				mock := NewMockSchemaStore(ctrl)

				s, _ := NewSchema(koValidator)

				mock.EXPECT().SchemaFor(ctx, "zdocs", "tst").Return(s, nil)

				return mock
			},
			blockStore: func() *MockBlockStore { return NewMockBlockStore(ctrl) },
			err:        assert.Error,
		},
		{
			description: "when schema repository returns a schema, doc is valid, store fails",
			collection:  "zdocs",
			schemaStore: func() *MockSchemaStore {
				mock := NewMockSchemaStore(ctrl)

				s, _ := NewSchema(okValidator)

				mock.EXPECT().SchemaFor(ctx, "zdocs", "tst").Return(s, nil)

				return mock
			},
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().StoreBlockRecords(ctx, []BlockRecord{
					{
						ID:          "12",
						Section:     "tst",
						Collection:  "zdocs",
						DocumentID:  "12345",
						Version:     11,
						Payload:     []byte(`{"references":[{"code":12},{"code":13]}`),
						IndexTuples: []IndexTuple{},
						Digest:      dig,
					},
				}).Return(fmt.Errorf("failed"))

				return mock
			},
			err: assert.Error,
		},
		{
			description: "when schema repository returns a schema, doc is valid and store succeeds",
			collection:  "zdocs",
			schemaStore: func() *MockSchemaStore {
				mock := NewMockSchemaStore(ctrl)

				s, _ := NewSchema(okValidator)

				mock.EXPECT().SchemaFor(ctx, "zdocs", "tst").Return(s, nil)

				return mock
			},
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().StoreBlockRecords(ctx, []BlockRecord{
					{
						ID:          "12",
						Section:     "tst",
						Collection:  "zdocs",
						DocumentID:  "12345",
						Version:     11,
						Payload:     []byte(`{"references":[{"code":12},{"code":13]}`),
						IndexTuples: []IndexTuple{},
						Digest:      dig,
					},
				}).Return(nil)

				return mock
			},
			err: assert.NoError,
		},
		{
			description: "when schema repository returns a schema, doc is valid, indexer defined and succeeds",
			collection:  "zdocs",
			schemaStore: func() *MockSchemaStore {
				mock := NewMockSchemaStore(ctrl)

				s, _ := NewSchema(okValidator, WithIndexes(
					Index{
						Name:   "uk",
						Unique: true,
						Path:   "references",
						Fields: []string{"code"},
					},
				))

				mock.EXPECT().SchemaFor(ctx, "zdocs", "tst").Return(s, nil)

				return mock
			},
			blockStore: func() *MockBlockStore {
				mock := NewMockBlockStore(ctrl)

				mock.EXPECT().StoreBlockRecords(ctx, []BlockRecord{
					{
						ID:         "12",
						Section:    "tst",
						Collection: "zdocs",
						DocumentID: "12345",
						Version:    11,
						Payload:    []byte(`{"references":[{"code":12},{"code":13]}`),
						IndexTuples: []IndexTuple{{
							Slug:   "uk",
							Unique: true,
							Vals:   []string{"12", "13"},
						}},
						Digest: dig},
				}).Return(nil)

				return mock
			},
			err: assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			collection, err := newCollection(tc.collection, tc.blockStore(), tc.schemaStore())
			if err != nil {
				t.Error("failed with", err)
			}

			err = collection.StoreDocumentSections(ctx, "12345", Section{Slug: "tst", Blocks: []Block{eBlk}})
			tc.err(t, err, "should assert the error expectation")
		})
	}
}
