package veritas

import "errors"

// errors
var (
	ErrInvalidKey            = errors.New("invalid key")
	ErrInvalidSchema         = errors.New("invalid schema")
	ErrInvalidPayload        = errors.New("invalid payload")
	ErrInvalidFact           = errors.New("invalid document fact")
	ErrInvalidDocument       = errors.New("invalid document")
	ErrInvalidCollectionName = errors.New("invalid collection name")
	ErrInvalidValidator      = errors.New("invalid validator")
	ErrInvalidDigester       = errors.New("invalid digester")
	ErrInvalidMapper         = errors.New("invalid mapper")
)
