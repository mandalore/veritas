# Veritas

Veritas is a storage system, modeled as a generic Document Store and that aims to provide the following features:

* Collection and Document Storage
    * Support for Versioned Collection Schemas
    * Support for Uniqueness constrainst.
    * Support for Primary and Secondary Document Key Reads
    * Read after Write Guarantees
    * Concurrency Control and Versioned Documents
* Archiving
    * Support for Audit Log based on CDC do Document Updates
    * Go SDK and REST API for seamless Audit Log consultations
* Indexing and Transformation
    * Support for Collection and Document Indexing on Read Storage essentially Search Storage for automatic Searchable Read Models.
    * Collection Schemas allow mapping definitions providing mappings and transformation feeds for external Streams like WISE Entity Feeds.
* Common APIs
    * Standart Go SDK for the Collection and Document Storage
    * GRPC Service for other languages
    * REST API
* Highly Scalable
    * Collections and Document Storage Abstraction
    * Storage Partitioning and Sharding
    * Separation of Write and Read Models

## Data Models

### Document 

```
type Document struct {
   Key      string
   Payload  struct {
     Data []byte
     Format string
   } 
   Headers  map[string][]Stringer
   Events   []Event
   Offset | Version | Sequence uint64
 
}
 
doc, err := offers.Get("13321312")
 
offer := NewFromDoc(doc)
 
// Do stuff qith offer
 
doc.Payload(offer.ToPayload)
 
offers.Store(ctx, doc)
```

### APIs and Interfaces

```
API's and Interfaces
offers := veritas.CollectionFor("namespace", "offers")
 
offers.Store(ctx, key, payload, headers, events)
 
offers.Get(ctx, key)
```

## Services Provided

## Pre-Stream

* Store - Stores Schemas and Documents all by a Collection Slug.
* Forwarder - CDC that fetches updates and publishes them on a Kafka Queue
* Rebuilder - When a Schema Change ... tries to rebuild a Collection in the background, a fluffer (v2.0)

## Post-Stream

* Archiver - Archives all document updates
* Indexer - Indexes to a Search Database all searcheable Documents
* Documenter - Receives Schema updates and Generates documentation and Samples

## Directory Struture

At the top all these services that can be called by suppying config options, example:

veritas.NewStore(uri, options)

Where uri as a string compatible for a database connection, supported drivers:

* Postgres (v1)
* Cockroachdb (v1)

