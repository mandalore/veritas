package veritas

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewSection(t *testing.T) {
	_, err := NewSection("")
	assert.Error(t, err, "should return an error when section id is invalid")

	d, err := NewSection("offers")
	assert.NoError(t, err, "should not return an error when section has only a valid id")
	assert.Equal(t, Section{Slug: "offers"}, d, "should have correctly created the section")

	d, err = NewSection("offers", Block{})
	assert.Error(t, err, "should return an error when receiving an invalid block")

	d, err = NewSection("offers", Block{ID: "123"})
	assert.Error(t, err, "should return return an error when receiving an valid block")

	d, err = NewSection("offers", Block{ID: "123", Payload: []byte(`a`)})
	assert.NoError(t, err, "should return return an error when receiving an valid block")
}

func TestNewBlock(t *testing.T) {
	_, err := NewBlock("", []byte("studd"))
	assert.Error(t, err, "should return an error when block id is invalid")

	d, err := NewBlock("12", nil)
	assert.Error(t, err, "should not return an error when payload is zero")

	d, err = NewBlock("12", []byte("studd"))
	assert.NoError(t, err, "should not return an error when document has key and payload")
	assert.Equal(t, Block{ID: "12", Payload: []byte("studd")}, d, "should have correctly created the document")

	d, err = NewBlock("12", []byte("studd"), WithBlockVersion(10))
	assert.NoError(t, err, "should not return an error when document has key and payload")
	assert.Equal(t, Block{ID: "12", Version: 10, Payload: []byte("studd")}, d, "should have correctly created the document")

	d, err = NewBlock("12", []byte("studd"), WithBlockVersion(10), WithBlockFacts("Event1", ""))
	assert.Error(t, err, "should return an error when a fact is invalid")

	d, err = NewBlock("12", []byte("studd"), WithBlockVersion(10), WithBlockFacts("Event1", "Event2"))
	assert.NoError(t, err, "should not return an error when document has key and payload")
	assert.Equal(t, Block{ID: "12", Version: 10, Payload: []byte("studd"), Facts: []string{"Event1", "Event2"}}, d, "should have correctly created the document")
}
