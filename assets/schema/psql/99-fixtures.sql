CREATE TABLE tmap (
    tmap_id SERIAL NOT NULL,
    slug    TEXT NOT NULL,
    PRIMARY KEY(tmap_id),
    UNIQUE(slug)
);