package veritas

import (
	"fmt"
	"time"
)

type Document struct {
	ID         string
	Collection string
	Sections   []Section
}

type Section struct {
	Slug   string
	Blocks []Block
}

func NewSection(slug string, blocks ...Block) (Section, error) {
	s := Section{Slug: slug, Blocks: blocks}

	if err := s.Validate(); err != nil {
		return Section{}, err
	}

	return s, nil
}

func (s Section) Validate() error {
	if s.Slug == "" {
		return ErrInvalidKey
	}

	for _, b := range s.Blocks {
		if err := b.Validate(); err != nil {
			return fmt.Errorf("invalid block on section: %w", ErrInvalidKey)
		}
	}

	return nil
}

// Block a storable document
type Block struct {
	ID      string
	Version uint64
	Payload []byte
	Facts   []string
}

// BlockOptional type alias for first order function that add optional attributtes to a Block
type BlockOptional func(d *Block) error

// WithBlockVersion first order function to use as an optional parameter for the NewBlock factory method
// this function adds an optional Version
func WithBlockVersion(v uint64) BlockOptional {
	return func(d *Block) error {
		d.Version = v

		return nil
	}
}

// WithBlockFacts first order function to use as an optional parameter for the NewBlock factory method
// this function adds optional Facts to the Block
func WithBlockFacts(facts ...string) BlockOptional {
	return func(d *Block) error {
		for _, fact := range facts {
			if fact == "" {
				return ErrInvalidFact
			}

		}

		d.Facts = facts

		return nil
	}
}

//NewBlock factory method creation a new Veritas Docuument
func NewBlock(id string, p []byte, opts ...BlockOptional) (Block, error) {
	d := Block{ID: id, Payload: p}

	for _, optFn := range opts {
		if err := optFn(&d); err != nil {
			return d, err
		}
	}

	return d, d.Validate()
}

// Validate ...
func (d Block) Validate() error {
	if d.ID == "" {
		return ErrInvalidKey
	}

	if len(d.Payload) == 0 {
		return ErrInvalidPayload
	}

	return nil
}

// IndexTuple defines a returned IndexTuple
type IndexTuple struct {
	Slug   string
	Unique bool
	Vals   []string
}
type BlockRecord struct {
	ID            string
	Version       uint64
	Collection    string
	DocumentID    string
	Section       string
	Payload       []byte
	Facts         []string
	SchemaVersion uint32
	IndexTuples   []IndexTuple
	Digest        []byte
	Deleted       bool
	UpdatedAt     time.Time
}

func NewBlockRecord(schema Schema, collection, dID, section string, block Block) (BlockRecord, error) {
	rec := BlockRecord{
		ID:         block.ID,
		Version:    block.Version,
		Collection: collection,
		DocumentID: dID,
		Section:    section,
		Payload:    block.Payload,
		Facts:      block.Facts,
	}

	digest, err := schema.Digest(block)
	if err != nil {
		return rec, err
	}

	idxs, err := schema.IndexesFor(block)
	if err != nil {
		return rec, err
	}

	rec.SchemaVersion = schema.Version
	rec.IndexTuples = idxs
	rec.Digest = digest

	return rec, nil
}
