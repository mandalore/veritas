package veritas

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type okDigester struct{}
type koDigester struct{}
type koMapper struct{}
type okMapper struct{}

func (okd okDigester) Digest(p []byte) ([]byte, error) {
	return []byte("11"), nil
}

func (kod koDigester) Digest(p []byte) ([]byte, error) {
	return nil, fmt.Errorf("i have failed")
}

func (okm okMapper) Map(b Block) (map[string]interface{}, error) {
	return map[string]interface{}{
		"references": []string{"123", "321"},
	}, nil
}

func (kom koMapper) Map(b Block) (map[string]interface{}, error) {
	return nil, errors.New("boom")
}

func TestNewSchema(t *testing.T) {
	okv, err := NewValidator("acceptall", []byte{})
	if err != nil {
		t.Error(err)
	}

	_, err = NewSchema(Validator{})
	assert.Error(t, err, "should return an error when validator is nil")

	s, err := NewSchema(okv)
	assert.NoError(t, err, "should not return an error when all required params are passed")
	assert.Equal(t, Schema{
		validator: okv,
		indexes:   []Index{},
		digester:  defaultDigester{},
		mappings:  map[string]Mapper{},
	}, s, "should have populated the schema correctly")

	s, err = NewSchema(okv, WithDigester(nil))
	assert.Error(t, err, "should return an error when all required params are passed, optional nil digester passed")

	s, err = NewSchema(okv, WithDigester(okDigester{}))
	assert.NoError(t, err, "should not return an error when all required params are passed, and optional valid digester passed")
	assert.Equal(t, Schema{
		validator: okv,
		indexes:   []Index{},
		digester:  okDigester{},
		mappings:  map[string]Mapper{},
	}, s, "should have populated the schema correctly")

	s, err = NewSchema(okv, WithDigester(okDigester{}), WithMapper("", okMapper{}))
	assert.Error(t, err, "should not return an error when all required params are passed, and optional valid digester passed, but invalid mapper - missing slug")

	s, err = NewSchema(okv, WithDigester(okDigester{}), WithMapper("as", nil))
	assert.Error(t, err, "should not return an error when all required params are passed, and optional valid digester passed, but invalid mapper - missing slug")

	s, err = NewSchema(okv, WithDigester(okDigester{}), WithMapper("skeys", okMapper{}), WithMapper("wise", koMapper{}))
	assert.NoError(t, err, "should not return an error when all required params are passed, and optional valid digester and mappings passed")
	assert.Equal(t, Schema{
		validator: okv,
		indexes:   []Index{},
		digester:  okDigester{},
		mappings: map[string]Mapper{
			"skeys": okMapper{},
			"wise":  koMapper{},
		},
	}, s, "should have populated the schema correctly")

	s, err = NewSchema(okv, WithDigester(okDigester{}), WithIndexes(Index{Name: "cenas", Unique: true, Path: "zee_id"}), WithMapper("wise", okMapper{}))
	assert.NoError(t, err, "should not return an error when all required params are passed, and optional valid digester and mappings passed")
	assert.Equal(t, Schema{
		validator: okv,
		indexes: []Index{
			{Name: "cenas", Unique: true, Path: "zee_id"},
		},
		digester: okDigester{},
		mappings: map[string]Mapper{
			"wise": okMapper{},
		},
	}, s, "should have populated the schema correctly")
}

func TestValidate(t *testing.T) {
	okv, err := NewValidator("acceptall", []byte{})
	if err != nil {
		t.Error(err)
	}

	s, err := NewSchema(okv)
	if err != nil {
		t.Error(err)
	}

	err = s.Validate(Block{})
	assert.NoError(t, err, "should not return an error when internal validator succeeds")

	kov, err := NewValidator("denyall", []byte{})
	if err != nil {
		t.Error(err)
	}

	s, err = NewSchema(kov)
	if err != nil {
		panic(err)
	}

	err = s.Validate(Block{})
	assert.Error(t, err, "should return an error when internal validator fails")
}

func TestDigest(t *testing.T) {
	okv, err := NewValidator("acceptall", []byte{})
	if err != nil {
		t.Error(err)
	}

	s, err := NewSchema(okv)
	if err != nil {
		t.Error(err)
	}

	d, err := s.Digest(Block{})
	assert.NoError(t, err, "should not return an error when no digest is defined")
	assert.Equal(t, []byte{
		227, 176, 196, 66, 152, 252, 28, 20, 154, 251, 244, 200, 153, 111, 185, 36, 39, 174, 65, 228, 100, 155, 147, 76, 164, 149, 153, 27, 120, 82, 184, 85,
	}, d, "should have returned a valid digest, using the default digester")

	s, err = NewSchema(okv, WithDigester(okDigester{}))
	if err != nil {
		panic(err)
	}

	d, err = s.Digest(Block{})
	assert.NoError(t, err, "should not return an error when a custom digest")
	assert.Equal(t, []byte{49, 49}, d, "should have returned a valid digest, using a custom digester")

	s, err = NewSchema(okv, WithDigester(koDigester{}))
	if err != nil {
		panic(err)
	}

	_, err = s.Digest(Block{})
	assert.Error(t, err, "should return an error when a custom digest fails to generate a digest")
}

func TestMap(t *testing.T) {
	okv, err := NewValidator("acceptall", []byte{})
	if err != nil {
		t.Error(err)
	}

	s, err := NewSchema(okv)
	if err != nil {
		t.Error(err)
	}

	kv, err := s.Map("cenas", Block{})
	assert.NoError(t, err, "should not return an error when no map is defined for the slug")
	assert.Nil(t, kv, "should return an empty map")

	s, err = NewSchema(okv, WithMapper("cenas", okMapper{}))
	if err != nil {
		panic(err)
	}

	kv, err = s.Map("cenas", Block{})
	assert.NoError(t, err, "should not return an error when a map is defined for the slug")
	assert.Equal(t, map[string]interface{}{
		"references": []string{"123", "321"},
	}, kv, "should return the expected mapping, when mapper can map")

	s, err = NewSchema(okv, WithMapper("cenas", koMapper{}))
	if err != nil {
		panic(err)
	}

	_, err = s.Map("cenas", Block{})
	assert.Error(t, err, "should  return an error since mapper fails")
}

func TestIndexes(t *testing.T) {
	okv, err := NewValidator("acceptall", []byte{})
	if err != nil {
		t.Error(err)
	}

	s, err := NewSchema(okv, WithIndexes(
		Index{
			Name:   "product_id",
			Unique: false,
			Fields: []string{"product_id"},
		},
		Index{
			Name:   "product.references",
			Unique: true,
			Path:   "references",
			Fields: []string{"type", "value"},
		},
		Index{
			Name:   "personal",
			Unique: false,
			Path:   "contact",
			Fields: []string{"first", "last"},
		},
	))
	if err != nil {
		panic(err)
	}

	testCases := []struct {
		description string
		block       Block
		expected    []IndexTuple
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when document has fields for single index",
			block: Block{
				Payload: []byte(`{"product_id":"123"}`),
			},
			expected: []IndexTuple{
				{Slug: "product_id", Vals: []string{"123"}},
				{Slug: "product.references", Unique: true, Vals: []string{}},
				{Slug: "personal", Vals: []string{}},
			},
			err: assert.NoError,
		},
		{
			description: "when document has fields for single index, empty array on multi-field",
			block: Block{
				Payload: []byte(`{"product_id":"123","references":[]}`),
			},
			expected: []IndexTuple{
				{Slug: "product_id", Vals: []string{"123"}},
				{Slug: "product.references", Unique: true, Vals: []string{}},
				{Slug: "personal", Vals: []string{}},
			},
			err: assert.NoError,
		},
		{
			description: "when document has fields for multi-field index from an array",
			block: Block{
				Payload: []byte(`{"references":[{"type":"EAN","value":"001"}, {"type":"EAN", "value" :"120"}]}`),
			},
			expected: []IndexTuple{
				{Slug: "product_id", Vals: []string{}},
				{Slug: "product.references", Unique: true, Vals: []string{"EAN::001", "EAN::120"}},
				{Slug: "personal", Vals: []string{}},
			},
			err: assert.NoError,
		},
		{
			description: "when document has fields for multi-field index from multiple fields",
			block: Block{
				Payload: []byte(`{"product_id":"123","references":[{"type":"EAN","value":"001"}, {"type":"EAN", "value" :"120"}]}`),
			},
			expected: []IndexTuple{
				{Slug: "product_id", Vals: []string{"123"}},
				{Slug: "product.references", Unique: true, Vals: []string{"EAN::001", "EAN::120"}},
				{Slug: "personal", Vals: []string{}},
			},
			err: assert.NoError,
		},
		{
			description: "when document uses an array field on a scalar path, an error should occur",
			block: Block{
				Payload: []byte(`{"product_id":[123,135],"references":[{"type":"EAN","value":"001"}, {"type":"EAN", "value" :"120"}]}`),
			},
			expected: []IndexTuple{},
			err:      assert.Error,
		},
		{
			description: "when document uses an array field on an array path, an error should occur",
			block: Block{
				Payload: []byte(`{"references":[{"type":"EAN","value":["001","002"}, {"type":"EAN", "value" :"120"}]}`),
			},
			expected: []IndexTuple{},
			err:      assert.Error,
		},

		{
			description: "when generating index for object using multiple fields",
			block: Block{
				Payload: []byte(`{"contact":{"first":"Nuno","last":"Mota"}}`),
			},
			expected: []IndexTuple{
				{Slug: "product_id", Vals: []string{}},
				{Slug: "product.references", Unique: true, Vals: []string{}},
				{Slug: "personal", Vals: []string{"Nuno::Mota"}},
			},
			err: assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			idx, err := s.IndexesFor(tc.block)

			tc.err(t, err, "should have returned the expected error assertion")

			assert.Equal(t, tc.expected, idx)
		})
	}
}

func TestSerialization(t *testing.T) {
	def := []byte(`{"type":"record","namespace":"","name":"products","fields":[{"name":"product_id","type":"string"},{"name":"references","type":"array","items":{"type":"record","name":"reference","fields":[{"name":"type","type":"long"},{"name":"value","type":"string"}]}}]}`)

	v, err := NewValidator("avro", def)
	if err != nil {
		t.Error(err)
	}

	s, err := NewSchema(v, WithIndexes(
		Index{
			Name:   "product_id",
			Unique: false,
			Fields: []string{"product_id"},
		},
		Index{
			Name:   "references",
			Unique: true,
			Path:   "references",
			Fields: []string{"type", "value"},
		},
	))
	if err != nil {
		t.Error(err)
	}

	d, err := json.Marshal(s)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, []byte(`{"version":0,"validator":{"typ":"avro","def":{"type":"record","namespace":"","name":"products","fields":[{"name":"product_id","type":"string"},{"name":"references","type":"array","items":{"type":"record","name":"reference","fields":[{"name":"type","type":"long"},{"name":"value","type":"string"}]}}]}},"indexes":[{"Name":"product_id","Unique":false,"Path":"","Fields":["product_id"]},{"Name":"references","Unique":true,"Path":"references","Fields":["type","value"]}]}`), d, "should have returned the expected interface")

	uv := Schema{}

	err = json.Unmarshal(d, &uv)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, s.Version, uv.Version, "deserialised should produce equal schema version")
	assert.Equal(t, s.mappings, uv.mappings, "deserialised should produce equal schema mappings")
	assert.Equal(t, s.indexes, uv.indexes, "deserialised should produce equal schema indexes")

	assert.Equal(t, s.validator.typ, uv.validator.typ, "deserialised should produce equal schema validator")
	assert.Equal(t, s.validator.def, uv.validator.def, "deserialised should produce equal schema validator")

	err = s.Validate(Block{Payload: []byte(`{}`)})
	assert.Error(t, err, "should return an error with initial validator")

	err = s.Validate(Block{Payload: []byte(`{"product_id":"123","references":[{"type":12,"value":"001"}]}`)})
	assert.NoError(t, err, "should not return an error with initial validator")

	err = uv.Validate(Block{Payload: []byte(`{}`)})
	assert.Error(t, err, "should return an error with unserialised validator")

	err = uv.Validate(Block{Payload: []byte(`{"product_id":"123","references":[{"type":12,"value":"001"}]}`)})
	assert.NoError(t, err, "should not return an error with unserialised validator")
}
