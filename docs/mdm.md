# RFC: Veritas Schema Format

Schema definition using JSON

```json
	{
		"type"      : "record",
		"namespace" : "iron",
		"name"      : "product",
		"fields" : [
			{
				"name": "product_id", 
				"type": "string"
			},
			{
				"name": "references", 
				"type": "array", 
				"items" : {
					"type": "record",
					"name": "reference",
					"fields" : [
						{"name": "type", "type": "long"},
						{"name": "value", "type": "string"}
					]
				}
			}
		]
	}


{
	"version": "json#1",
	"properties": [
		{
            "code": "",
			"type": "string",
			"path": "update_history.0.status",
			"validate": "nonzero",
			"search": {
				"field": "current_status",
				"tr": ["lowercase"],
				"map": {"original value": "destination value"},
			}
		}
	]
}
```

- `type` of the value, which must be an atomic type: string, integer, decimal, boolean or array versions of these.
- `path` is the source field in the message payload using [gjson path syntax](https://github.com/tidwall/gjson#path-syntax)
- `validate` field uses [Validator like syntax](https://github.com/go-validator/validator) or similar.
- `search` is configuration for search time
  - `field` is the search field which is filled in with the value from the property after transformations and mappings.
  - `tr` is a list of transformations to apply to the original value
  - `map` is a direct value to value mapping