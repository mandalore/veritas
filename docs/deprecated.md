## Veritas

Veritas is a subpackage oriented event store, it works both as a remote event store using it's exported REST API's and additionally exports packages so it can be used as an SDK inside external go packages.

Veritas is composed by several smaller packages:

* Log - The log package exposes the ability to manage Collections and Documents.
* Archiver - the archiver package handles log and stream compactation procedures.
* Projector - the projector package handles all Event Projections based on the standart event format
* Schema Registry - allows the system to add schemas for mapping and collection validation
* API - The API package runs and exposes all of the above packages as a single cohesive REST API.


### Events and Anotations

For Veritas the events it stores and fetches have limited meaning, they essentially hold meta-data to perform tracing and identification.

```
type Event struct {
	ID            uuid.UUID
	Type          string
	Annotations   Annotations
	Serialization Serialization
}

type Annotations map[string][]string

type Serialization struct {
	Format string
	Raw    []byte
}
```

Essentially an event has a unique ID a Type describing what this event did, how it is serialized and the actual serialization.
All the other data assotiating the Event to a Topic or Stream is feeded or fetched via SDK Usage.

What Veritas provides other than this simple data structure is the possibility to anotate the events, with additional meta-data, this meta data can be used later either by the event processors or Veritas Projectors to generate Read-Projection for faster searches on the Events Store (Check Projections).

## SDK

The veritas SDK is split onto packages, the basic functionality of veritas implements a simple evenstore.

### Creating a New Log

```
log := veritas.NewLog(storage, archive)

```
```
collection, err := log.Collection("products")

expectedOffset := 120

err := collection.Store("123", 120, events)

event := []Event{Event{Type:"cenas", Data:"cenas_as"}}

collection, err := collection.Segment("234", Cursor{ Position: 0, Limit: 0 })
collection, err := collection.History("123")
collection, err := collection.Last("123")
```


### Advanced Use Cases

#### Use Case - Snapshot with Domain Events

Snapshot based streams always keep a serialized version of the Entity they store. But sometimes Commands Applied to an Entity or Aggregate need to Publish DomainEvents, to implement this the recomended solutions is to use the Envelope Meta Data Appropriatly.

So on these cases, instead of calling the Event `Entity.Updated` name it with the exact thing that has happened for example `Entity.CompletedRegistratiom` with this information and the full external systems can infer and only listen to the correct events they need to.

Additionally you can use the Event Meta Data to add additional information detailing relevant information about this snapshot and the deltas it produced.

