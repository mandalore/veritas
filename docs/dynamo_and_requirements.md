# Features

## Round Trip and Colunar mappings
 
Collections have schemas these schemas are optionally:
 
* strict
* loose
 
### Strict
 
Document payload must match schema definition and all not mapped fields should be ignored.
 
### Loose
 
Document payload validates schema and additional fields should not be lost.
 
Consider storing these extra fields on a separate attribute on the Structure aka DOM
 
 
## Colunar Mappings
 
Collections define per a particular set of documents:
 
* It's Schema - for validation
* It's Mappings
                * for internal storage driver generation
                * for external transformations aka publish the public version of the stored document.
 
The mappings are resposible for identifying which fields should:
 
* Be mapped into specific columns on a table storage (Dynamo or RDS)
* Which fields (or composition of fields) should be Unique
* Which fields (or composition of fields) should be Indexed
 
{
    "columns" : [
        {
            "column" : "email", // can be name id defines the column name
            "selector" : "friends.#(last=="Murphy")#.first" // used this for inspiration https://github.com/tidwall/gjson,
            "type" : "string", // for column definition
    ...
        },
        ...
     ],
     "keys" : {
        { columns => [ "email", "company" ], unique => true }
     }  
}
 
**Note:** for Dynamo consider cost implications of global and secondary indexes to assure uniqueness and foreign key search
**Note:** Table structure and Mapping can be static Go code generated from the Schema Definition and Mappings
 
## Dynamo
 
Use Dynamo for initial Storage Driver, things to consider:
 
* How to assure non PK Uniqueness ??
    * External Dynamotable + Dynamo Transactions ??
* Versioning is not an Issue it's a question of using the sort key and the document ID, we should use an attribute.
* Investigate CDC - Kinesis is guaranteed ... check for Lambda based CDC to inject on Kafka Streams.
 