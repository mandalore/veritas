# TODO:

1. Update Schema Model to new format.
2. Update Veritas Store to handle indexes format.
3. Handle Schema Marshaling and Unmarshalling
	3.1 Avro
	3.2 JSON Schema
4. Implement Schema Repository
	4.1 Schema Store
	4.2 In Memory Cache with Expiration Channel

5. (WITH TIME) Custom Veritas Validator

## How to define Schemas on JSON

A schema is composed by several pluggable components:

* Validator - A component that allows fo Collection Documents to be Validated
* Indexes - A Collection of elements of the Schema that need to have separate search indexes
* Mappers - A Collection of Mappers that transform a Document into a new structure

Although a simple Go API is provided a JSON representation for Schema loading is provided, example:

```
{
	"name" : "products"
	"validator" : {
		"type" : "avro",
		"def"  : { ... JSON AVRO DEFINITION ... }
	},
	indexes : [
		{
			name    => "product.items",
			unique  => true,
			columns => [
				"product.items.#.seller_id",
				"product.items.#.seller_code",
			]
		}
	],
	mappers : {
		"wise" : { ... lore definition ... }
	}
}
```

## Validators

Veritas accepts three types of Schema validators:

* Avro JSON Schema
* JSON Schema
* Veritas Schema

Only Veritas Schema provides an SDK for construction.


### Veritas Schema Definition

**Golang Field Definition**

```
type Field struct {
	typ  FieldType
	name string

	required bool
	nullable bool
	unique   bool

	items  FieldType
	fields []Field
}

**Example Schame Definition**

```
{
	"namespace" : "pim",
	"name"      : "product",
	"version":  : 23,
	"indexes" : {...} // Mappers um Mapper especifico 
	"fields":   [  // Validator
		// scalar type (string, int, bool, etc...)
		{
			"name" : "id",
			"type" : "string",
		},
		// scalar type (string, int, bool, etc...)
		{
			"name" : "email",
			"type" : "string",
			"constraint" : {
				"required" : true,
				"ukIndex" : true, // ISTO QUER DIZER QUE É SEC KEY
			}
		},
        // object type
		{
			"name" : "supplier",
			"type" : "object",
			"fields" : [ ... ],
		},
		// array of objects
		{
			"name" : "items",
			"type" : "array",
			"items" : {
				"type" : "object",
				"fields" : [ ... ]
			}
		}
		// array of strings
		{
			"name" : "items",
			"type" : "array",
			"items" : {
				"type" : "string"
			}
		}
		// map of objects
		{
			"name" : "items",
			"type" : "map",
			"items" : {
				"type" : "object",
				"fields" : [ ... ]
			}
		}
	]
}
```