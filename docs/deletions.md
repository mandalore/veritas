## Metadata Store and Document Store

## How to handle Deletes

Deletes are the same thing as Stores, they work because on store facts are wrapped with meta-data, example:

```
type Meta struct {
    Deleted   bool
    UpdatedAt time.Time
    Issuer    uuid.UUID
    TraceID ...
    SpanID ...
}

type KDocument struct {
    document Document
    headers  []Header
    facts   []Facts
    meta     Meta
}
```

When performin a Deletion Meta.Deleted is marked as bool, and:

* Delete state is propagate to the Read Store aka Elastic
* Get By Key works if defined on the Collection Settings
* When Storing a Document with the same Key as a previously deleted one, two things can happen:
    * When collection settings are set as squash, the old document will be deleted
    * When collection setting are not set as squash, the operation will return an error

* All References will be lost
* An Additional WISE Fact will be published notifying a DocumentDeletetion.
