# Veritas Search

> WIP

A generic specification for indexing and searching entities.

Search configuration must define

- Which fields to extract and index
- All fields must specify the data type for indexing

## Document specification

Document for storing in Elasticsearch.

```json
{
  "meta": {
    "namespace": "<namespace>",
    "type": "<entity type>",
    "schema": "<schema name>",
    "id": "<entity id>",
    "version": 1,
    "refs": {
      "<key>": ["<val>", ...]
    }
  },
  "data": {},

  // 

  "fields": {
    "txt": { "<key>": "value" },
    "int": { "<key>": 10 },
    "dec": { "<key>": 10.0001 },
    "cur": { "<key>": "10.00" },
    "geo_point": { "<key>": { "lat": 35.10012, "lon": 8.191231 } }, // index as geolocated point
    "ip_addr": { "<key>": "127.0.0.1" }, // index as an IP
    "terms": { "<key>": "value" } // index as keyword
  },
  "labels": {
    "<namespace>": ["tag", ...],
  }
}
```

## Index naming

Indexes are automatically created based on a template.

Prefix: `veritas-core-<env>-<namespace slug>-<entity type slug>-<entity schema slug>`

Sluggify rules:

1. convert all characters to lowercase
1. convert sequences of non [a-z0-9] characters to a single `_` character.

## Index template

```json
{
}
```

## Ideas

### Dymo

Allow specifying dymo rules to generate tags.