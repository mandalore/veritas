A Document is defined by:

```
	Key     []byte
	Version uint32
	Facts   []string
	Payload []byte
```

A Schema by:

```
{
	"name" : "products"
	"validator" : {
		"type" : "avro",
		"def"  : { ... JSON AVRO DEFINITION ... }
	},
	indexes : [
		{
			name    => "product.items",
			unique  => true,
			columns => [
				"product.items.#.seller_id",
				"product.items.#.seller_code",
			]
		}
	],
	mappers : {
		"wise" : { ... lore definition ... }
	}
}
```

A Document belongs to a Collection, the ideia is breaking a Document into Blocks.

Example Availability Bucket:

Instead of Saving Documents why don't we save Blocks, Blocks can have:

* Schema
* Facts ["associated changes"] saved as Raw should be moved to a type

A Block has:

* A Group Identifier - For Example "offers"
* A Block Identifier - A Unique Identifier within the Group
* A Version 

Collection:
	Document:
		Section:
			* Block1
			* Block2
			* Block3
		Section
			* Block1

veritas.Store("collection", documentID, block1, block2)
veritas.Get("collection", documentID)
veritas.Get("collection", documentID, ["block1", "block2"])

Now the Document is the aggregator or Hash Key.

HK: DocumentID
SK: BlockType::BlockID

Availability Events, also stored as blocks:

* Bucket Document : [item, location]
    * Block v1::counter: { counter blobs } + version
    * Block DOC::ID, v1, committed, etc ... evt 1 (version X)
    * Block DOC::ID, v1, committed, etc ... evt 2 (version Y)



