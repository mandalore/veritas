package tests

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	// pq driver import
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mandalore/veritas"
)

// GetTestPool database Returns a Testing DB Pool
func GetTestPool(database string) (*sql.DB, error) {
	address := GetPqAddress()
	address = fmt.Sprintf("postgres://postgres:postgres@%s/%s?sslmode=disable", address, database)

	return sql.Open("postgres", address)
}

// GetPqAddress helper method to get the mysql address for testing
func GetPqAddress() string {
	address := os.Getenv("POSTGRES_ADDRESS")
	if address == "" {
		address = "127.0.0.1:5432"
	}

	return address
}

// GetDynamoAddress helper method to get the dynamo address for testing
func GetDynamoAddress() string {
	address := os.Getenv("DYNAMODB_ADDRESS")
	if address == "" {
		address = "http://localhost:8000"
	}

	return address
}

// GetDynamoPool ...
func GetDynamoPool() (*dynamodb.DynamoDB, error) {
	os.Setenv("AWS_REGION", "eu-west-1")
	os.Setenv("AWS_ACCESS_KEY_ID", "local")
	os.Setenv("AWS_SECRET_ACCESS_KEY", "local")

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return dynamodb.New(sess, &aws.Config{Endpoint: aws.String(GetDynamoAddress())}), nil
}

// BootStrapDynamo ...
func BootStrapDynamo() {
	client, err := GetDynamoPool()
	if err != nil {
		panic(err)
	}

	tableInput := &dynamodb.CreateTableInput{
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("key"),
				KeyType:       aws.String("HASH"),
			},
		},

		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("key"),
				AttributeType: aws.String("S"),
			},
		},

		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		TableName: aws.String("collections"),
	}

	_, err = client.DescribeTable(&dynamodb.DescribeTableInput{TableName: aws.String("collections")})
	if err == nil {
		_, err := client.DeleteTable(&dynamodb.DeleteTableInput{TableName: aws.String("collections")})
		if err != nil {
			log.Println(err)
		}
	}

	_, err = client.CreateTable(tableInput)
	if err != nil {
		panic(err)
	}
}

// AssertSchemas ...
func AssertSchemas(t *testing.T, e, a veritas.Schema) {
	se, err := json.Marshal(e)
	if err != nil {
		t.Error("invalid expected schema")
	}

	sa, err := json.Marshal(e)
	if err != nil {
		t.Error("invalid actual schema")
	}

	assert.Equal(t, se, sa, "schemas are Equal")
}
