package main

import (
	"context"
	"database/sql"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/mandalore/veritas/tests"
)

var cache = map[string]string{}

// LoadSQL loads the given schema and runs it agaisnt the database
func LoadSQL(db *sql.DB, path string) (err error) {
	var ctx = context.Background()

	file, ok := cache[path]
	if !ok {
		var values []byte
		values, err = ioutil.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}
		cache[path] = string(values)
		file = string(values)
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	sql := strings.TrimSuffix(strings.TrimSpace(string(file)), ";")
	requests := strings.Split(sql, ";")
	for _, request := range requests {
		_, err = db.ExecContext(ctx, request)
		if err != nil {
			log.Printf("Failed to process request (%s): %s\n", path, request)
			return err
		}
	}

	return nil
}

func main() {
	cleanupDatabases()
	loadDatabaseSchemas()
	loadDatabaseFixtures()
}

func sqlPath() (string, error) {
	return os.Getwd()
}

func cleanupDatabases() {
	pool, err := tests.GetTestPool("postgres")
	if err != nil {
		log.Fatal("Could not connect to the database", err)
	}
	defer pool.Close()

	wd, err := sqlPath()
	if err != nil {
		log.Fatal(err)
	}

	if err := LoadSQL(pool, wd+"/assets/schema/psql/00-cleanup.sql"); err != nil {
		log.Fatal(err)
	}
}

func loadDatabaseSchemas() {
	pool, err := tests.GetTestPool("veritas")
	if err != nil {
		log.Fatal("Could not connect to the database", err)
	}
	defer pool.Close()

	wd, err := sqlPath()
	if err != nil {
		log.Fatal(err)
	}

	if err := LoadSQL(pool, wd+"/assets/schema/psql/01-schema.sql"); err != nil {
		log.Fatal(err)
	}
}

func loadDatabaseFixtures() {
	pool, err := tests.GetTestPool("veritas")
	if err != nil {
		log.Fatal("Could not connect to the database", err)
	}
	defer pool.Close()

	wd, err := sqlPath()
	if err != nil {
		log.Fatal(err)
	}

	if err := LoadSQL(pool, wd+"/assets/schema/psql/99-fixtures.sql"); err != nil {
		log.Fatal(err)
	}
}
