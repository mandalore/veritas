package veritas

import (
	"context"
)

// SchemaStore interface for the schema repository
type SchemaStore interface {
	Store(ctx context.Context, collection, section string, schema Schema) error
	SchemaFor(ctx context.Context, collection, section string) (Schema, error)
}

// BlockStore interface for the document repository
type BlockStore interface {
	StoreBlockRecords(ctx context.Context, records ...BlockRecord) error
	BlocksForDocument(ctx context.Context, collection string, dID string) ([]BlockRecord, error)
	BlocksForSections(ctx context.Context, collection string, dID string, sections []string) ([]BlockRecord, error)
	BlocksInSection(ctx context.Context, collection string, dID string, section string, bIDs []string) ([]BlockRecord, error)
}

// Store implementation of the event Store with steroids, aka fetching archived data
type Store struct {
	schemas SchemaStore
	blocks  BlockStore
}

// NewStore ...
func NewStore(bs BlockStore, sr SchemaStore) (Store, error) {
	s := Store{
		schemas: sr,
		blocks:  bs,
	}

	return s, nil
}

// Collection receives a slug and fetches the expected Collection
func (s Store) Collection(slug string) (Collection, error) {
	return newCollection(slug, s.blocks, s.schemas)
}
