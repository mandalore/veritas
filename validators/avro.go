package validators

import (
	"github.com/linkedin/goavro/v2"
)

// AvroValidator type holding an AvroValidator
type AvroValidator struct {
	codec *goavro.Codec
}

// NewAvroValidator constructor method to instanciate an AvroValidator
func NewAvroValidator(def []byte) (AvroValidator, error) {
	codec, err := goavro.NewCodec(string(def))
	if err != nil {
		return AvroValidator{}, err
	}

	return AvroValidator{codec: codec}, nil
}

// Validate implements the Validator interface
func (v AvroValidator) Validate(payload []byte) error {
	_, _, err := v.codec.NativeFromTextual(payload)
	if err != nil {
		return err
	}

	return nil
}
