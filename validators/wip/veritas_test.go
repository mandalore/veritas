package validators

/*
import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewScalar(t *testing.T) {
	tcs := []struct {
		desc      string
		typ       FieldType
		key       string
		expected  Field
		optionals []FieldOptional
		err       assert.ErrorAssertionFunc
	}{
		{
			desc:     "when creating a scalar and type is not supplied",
			key:      "ze",
			expected: Field{},
			err:      assert.Error,
		},
		{
			desc:     "when creating a scalar and key is not supplied",
			typ:      StringType,
			expected: Field{},
			err:      assert.Error,
		},
		{
			desc:     "when trying to create a scalar with an ARRAY type",
			typ:      ArrayType,
			key:      "zeeArray",
			expected: Field{},
			err:      assert.Error,
		},
		{
			desc:     "when trying to create a scalar with an MAP type",
			typ:      ObjectType,
			key:      "zeeMap",
			expected: Field{},
			err:      assert.Error,
		},
		{
			desc: "when creating scalar of string type",
			typ:  StringType,
			key:  "zeeStr",
			expected: Field{
				typ:      StringType,
				name:     "zeeStr",
				required: false,
				nullable: false,
				unique:   false,
				fields:   nil,
			},
			err: assert.NoError,
		},
		{
			desc: "when creating scalar of int type",
			typ:  IntegerType,
			key:  "zeeInt",
			expected: Field{
				typ:      IntegerType,
				name:     "zeeInt",
				required: false,
				nullable: false,
				unique:   false,
				fields:   nil,
			},
			err: assert.NoError,
		},
		{
			desc: "when creating scalar of bool type",
			typ:  BooleanType,
			key:  "zeeBool",
			expected: Field{
				typ:      BooleanType,
				name:     "zeeBool",
				required: false,
				nullable: false,
				unique:   false,
				fields:   nil,
			},
			err: assert.NoError,
		},
		{
			desc: "when creating scalar of decimal type",
			typ:  NumberType,
			key:  "zeeDecimal",
			expected: Field{
				typ:      NumberType,
				name:     "zeeDecimal",
				required: false,
				nullable: false,
				unique:   false,
				fields:   nil,
			},
			err: assert.NoError,
		},
		{
			desc:      "when creating scalar with optionals",
			typ:       NumberType,
			key:       "zeeDecimal",
			optionals: []FieldOptional{Required(true), Nullable(true), Unique(true)},
			expected: Field{
				typ:      NumberType,
				name:     "zeeDecimal",
				required: true,
				nullable: true,
				unique:   true,
				fields:   nil,
			},
			err: assert.NoError,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.desc, func(t *testing.T) {
			f, err := NewScalarField(
				tc.typ,
				tc.key,
				tc.optionals...,
			)

			tc.err(t, err, "should have returned the expected error")
			assert.Equal(t, tc.expected, f, "should have returned the expected field")
		})
	}
}
*/
