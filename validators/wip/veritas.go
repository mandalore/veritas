package validators

/*
import "fmt"

// FieldType type used for enumeration
type FieldType string

// Field Type enumeration
const (
	IntegerType  FieldType = "integer"
	StringType   FieldType = "string"
	BooleanType  FieldType = "boolean"
	NumberType   FieldType = "number"
	DateType     FieldType = "date"
	DateTimeType FieldType = "datetime"
	TimeType     FieldType = "time"

	ObjectType FieldType = "object"
	ArrayType  FieldType = "array"
	RecordType FieldType = "record"
)

{
    'name': 'birthday',
    'type': 'date',
    'format': 'default',
    'constraints': {
        'required': True,
        'minimum': '2015-05-30'
    }
}


// Field type holding all Fields and sub-fields of a Schema
type Field struct {
	typ  FieldType
	name string

	required bool
	nullable bool
	unique   bool

	items FieldItem
}

/*
// FieldItem ...
type FieldItem struct {
	typ    FieldType
	fields []Field
}

// FieldOptional first order helper function for optional field definitions
type FieldOptional func(f *Field) error

// Required optional field definition
func Required(o bool) FieldOptional {
	return func(f *Field) error {
		f.required = o

		return nil
	}
}

// Nullable optional field definition
func Nullable(o bool) FieldOptional {
	return func(f *Field) error {
		f.nullable = true

		return nil
	}
}

// Unique optional field definition
func Unique(o bool) FieldOptional {
	return func(f *Field) error {
		f.unique = true

		return nil
	}
}

// Fields defined optional record for complex types like maps or arrays
func Fields(fields ...Field) FieldOptional {
	return func(f *Field) error {
		f.fields = fields

		return nil
	}
}

// IsValid validates if current field is valid
func (f Field) IsValid() error {
	if f.typ == "" {
		return fmt.Errorf("field must be typed")
	}

	if f.name == "" {
		return fmt.Errorf("field must be named")
	}

	switch f.typ {
	case IntegerType, NumberType, BooleanType, StringType, DateType, DateTimeType:
		return f.isValidScalar()
	case ArrayType, ObjectType:
		return f.isValidRepeatable()
	}

	return nil
}

func (f Field) isValidScalar() error {
	return nil
}

func (f Field) isValidRepeatable() error {
	return nil
}

// NewScalarField factory method to define a new field
// examples:
//
// fname, err := NewScalarField(StringType, "first_name", Unique(t), Nullable(f))
//
// street, err := NewScalarField(StringType, "street", Unique(t), Nullable(f))
// county, err := NewScalarField(StringType, "county", Unique(t), Nullable(f))
//
// addr, err := NewScalarField(RecordType, "address", Record(street, county))
//
// addresses, err := NewScalarField(ArrayType, "adresses", Record(addr), )
// ))
func NewScalarField(ftype FieldType, name string, opts ...FieldOptional) (Field, error) {
	f := Field{typ: ftype, name: name}

	for _, opt := range opts {
		if err := opt(&f); err != nil {
			return f, err
		}
	}

	if err := f.IsValid(); err != nil {
		return Field{}, err
	}

	return f, nil
}

func NewRecordField(ftype FieldType, name string, opts ...FieldOptional) (Field, error) {
	f := Field{typ: ftype, name: name}

	for _, opt := range opts {
		if err := opt(&f); err != nil {
			return f, err
		}
	}

	if err := f.IsValid(); err != nil {
		return Field{}, err
	}

	return f, nil
}

// If type is Scalar children must be null
// if type is Array children is filled with an array of Attributes of Any type
// If type is Map children is filled with an array of Attributes of Any Type

// Represent complex structures with this structure
// How to represent maps and do we need too ? Maybe it's enough to map:
//    * secondary keys - path + value
//    * search keys

// Schema ...
type Schema struct {
	namespace string
	version   int32
	name      string
	fields    []Field
}

// NewSchema factory method to create and validate a new Schema definition
func NewSchema(ns, name string, version int32, fields ...Field) (Schema, error) {
	m := Schema{
		namespace: ns,
		name:      name,
		version:   version,
		fields:    fields,
	}

	return m, m.IsValid()
}

// IsValid validates the Schema
func (m Schema) IsValid() error {
	if m.namespace == "" || m.name == "" {
		return fmt.Errorf("namespace and name are required")
	}

	if m.version == 0 {
		return fmt.Errorf("version is required")
	}

	for _, field := range m.fields {
		if err := field.IsValid(); err != nil {
			return err
		}
	}

	return nil
}
*/
