package validators

import (
	"encoding/json"

	"github.com/qri-io/jsonschema"
)

// JSONSchemaValidator type holding an JSONSchemaValidator
type JSONSchemaValidator struct {
	rs jsonschema.RootSchema
}

// NewJSONSchemaValidator constructor method to instanciate an JSONSchemaValidator
func NewJSONSchemaValidator(def []byte) (JSONSchemaValidator, error) {
	v := JSONSchemaValidator{rs: jsonschema.RootSchema{}}
	if err := json.Unmarshal(def, &v.rs); err != nil {
		return JSONSchemaValidator{}, err
	}

	return v, nil
}

// Validate implements the Validator interface
func (v JSONSchemaValidator) Validate(payload []byte) error {
	if errors, _ := v.rs.ValidateBytes(payload); len(errors) > 0 {
		return errors[0]
	}

	return nil
}
