package validators

import "errors"

// AcceptAll validator that accepts any payload
type AcceptAll struct{}

// Validate validates a payload
func (v AcceptAll) Validate(p []byte) error {
	return nil
}

// DenyAll validator that will fail with any payload
type DenyAll struct{}

// Validate validates a payload
func (v DenyAll) Validate(p []byte) error {
	return errors.New("invalid document")
}
