package validators

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAvroValidate(t *testing.T) {
	def := []byte(`{}`)

	_, err := NewAvroValidator(def)
	assert.Error(t, err, "should return an error creating an avro validator from an invalid definition")

	def = []byte(`
	{
		"type": "record",
		"namespace" : "",
		"name": "products",
		"fields" : [
			{
				"name": "product_id", 
				"type": "string"
			},
			{
				"name": "references", 
				"type": "array", 
				"items" : {
					"type": "record",
					"name": "reference",
					"fields" : [
						{"name": "type", "type": "long"},
						{"name": "value", "type": "string"}
					]
				}
			}
		]
	}
	`)

	v, err := NewAvroValidator(def)
	assert.NoError(t, err, "should not return an error creating an avro validator from a valid definition")

	err = v.Validate([]byte(`{"product_id":"123"}`))
	assert.Error(t, err, "should return an error when payload is invalid")

	err = v.Validate([]byte(`{"product_id":"123","references":[{"type":12,"value":"001"}]}`))
	assert.NoError(t, err, "should not return an error when payload is valid")
}
