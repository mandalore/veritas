PACKAGES=$(shell go list ./... | grep -v /vendor/ | grep -v mock | grep -v /tests) 
AWS_REGION=eu-west-1
AWS_ACCESS_KEY_ID=local
AWS_SECRET_ACCESS_KEY=local 

check:
	@echo "go vet"
	go vet -all ./...

env:
	docker-compose up -d postgres dynamo

bootstrap:
	@echo "Boostraping database"
	@go run tests/boostrap/main.go

mocks:
	find . -name "mock" -type d -exec rm -rf {} +
	mockgen -source=store.go -destination=mock_store.go -package=veritas
	mockgen -source=storage/cached/schema_store.go -destination=storage/cached/mock_test.go -package=cached

local: env mocks bootstrap 
	@echo "Running local tests"
	@go test -race -short -cover $(PACKAGES)

test: install mocks bootstrap
	@echo "Running tests"
	@go test -coverprofile=.coverage.out -timeout 30s ./...
	go tool cover -func=.coverage.out | tail -1 

install:
	@go get ./...

local-test: env test

