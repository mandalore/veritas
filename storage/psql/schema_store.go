package psql

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/mandalore/veritas"
)

// SchemaStore the postgres SchemaStore implementation type
type SchemaStore struct {
	pool        *sql.DB
	collections *TableTypeMap
	sections    *TableTypeMap
}

// NewSchemaStore factory method
func NewSchemaStore(pool *sql.DB) (SchemaStore, error) {
	return SchemaStore{
		pool:        pool,
		collections: NewTableTypeMap("collections", WithIDCol("collection_id")),
		sections:    NewTableTypeMap("sections", WithIDCol("section_id")),
	}, nil
}

// Stores stores a new schema version
func (s SchemaStore) Store(ctx context.Context, collection, section string, schema veritas.Schema) error {
	if err := schema.IsValid(); err != nil {
		return err
	}

	collectionID, err := s.collections.IDFor(ctx, s.pool, collection)
	if err != nil {
		return fmt.Errorf("failed to get collection %s: %w", collection, err)
	}

	sectionID, err := s.sections.IDFor(ctx, s.pool, section)
	if err != nil {
		return fmt.Errorf("failed to get section %s: %w", section, err)
	}

	return inTransaction(ctx, s.pool, func(ctx context.Context, tx *sql.Tx) error {
		return s.store(ctx, tx, collectionID, sectionID, schema)
	})
}

func (s SchemaStore) store(ctx context.Context, tx *sql.Tx, collectionID, sectionID int, schema veritas.Schema) error {
	curr, err := s.versionFor(ctx, tx, collectionID, sectionID)
	if err != nil {
		return fmt.Errorf("failed to get version for %d: %w", collectionID, err)
	}

	if curr != schema.Version {
		return fmt.Errorf("conflict detected for %d expected %d got %d", collectionID, curr, schema.Version)
	}

	schema.Version++

	def, err := json.Marshal(schema)
	if err != nil {
		return fmt.Errorf("failed to serialize schema for collection_id %d: %w", collectionID, err)
	}

	if _, err = tx.ExecContext(ctx, `
		INSERT INTO schemas
		(collection_id, section_id,version, def)
		VALUES($1, $2, $3, $4)
	`, collectionID, sectionID, schema.Version, def); err != nil {
		return fmt.Errorf("failed to store schema %d: %w", collectionID, err)
	}

	return nil
}

func (s SchemaStore) versionFor(ctx context.Context, tx *sql.Tx, collectionID, sectionID int) (uint32, error) {
	var version uint32

	row := tx.QueryRowContext(ctx, `
		SELECT version 
		FROM schemas
		WHERE collection_id = $1 AND section_id = $2
		ORDER BY version DESC LIMIT 1
		FOR UPDATE NOWAIT
	`, collectionID, sectionID)

	if err := row.Scan(&version); err != nil {
		if err != sql.ErrNoRows {
			return version, errors.Wrap(err, "failed to fetch stream version")
		}
	}

	return version, nil
}

// SchemaFor(ctx context.Context, collection, block string) (Schema, error) - returns the latest Schema for a particular collection, block combination
func (s SchemaStore) SchemaFor(ctx context.Context, collection, section string) (veritas.Schema, error) {
	var (
		schema = veritas.Schema{}
		raw    []byte
	)

	collectionID, err := s.collections.IDFor(ctx, s.pool, collection)
	if err != nil {
		return schema, fmt.Errorf("failed to get collection %s: %w", collection, err)
	}

	sectionID, err := s.sections.IDFor(ctx, s.pool, section)
	if err != nil {
		return schema, fmt.Errorf("failed to get collection %s: %w", collection, err)
	}

	row := s.pool.QueryRowContext(ctx, `
		SELECT def
		FROM schemas
		WHERE collection_id = $1 AND section_id = $2
		ORDER BY version DESC LIMIT 1
	`, collectionID, sectionID)

	if err := row.Scan(&raw); err != nil {
		if err == sql.ErrNoRows {
			return schema, nil
		}

		return schema, err
	}

	if err := json.Unmarshal(raw, &schema); err != nil {
		return schema, err
	}

	return schema, nil
}
