package psql

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mandalore/veritas"
	"gitlab.com/mandalore/veritas/tests"
)

func initMockSchema(version uint32) veritas.Schema {
	def := []byte(`{"type":"record","namespace":"","name":"products","fields":[{"name":"product_id","type":"string"},{"name":"references","type":"array","items":{"type":"record","name":"reference","fields":[{"name":"type","type":"long"},{"name":"value","type":"string"}]}}]}`)

	v, err := veritas.NewValidator("avro", def)
	if err != nil {
		panic(err)
	}

	s, err := veritas.NewSchema(v, veritas.WithIndexes(
		veritas.Index{Name: "product_id", Unique: false, Path: "product_id"},
		veritas.Index{Name: "product.references", Unique: true, Path: "references", Fields: []string{"type", "value"}},
	))
	if err != nil {
		panic(err)
	}

	s.Version = version

	return s
}

func TestSchemaStore(t *testing.T) {
	ctx := context.Background()

	pool, err := tests.GetTestPool("veritas")
	if err != nil {
		panic("failed to get test pq DB")
	}

	collection := uuid.NewV4().String()
	section := uuid.NewV4().String()

	store, err := NewSchemaStore(pool)
	if err != nil {
		t.Error(err)
	}

	testCases := []struct {
		description string
		collection  string
		section     string
		toStore     veritas.Schema
		lastSchema  veritas.Schema
		schemas     []veritas.Schema
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when storing an invalid schema, should return an error and fail to save the schema",
			toStore:     veritas.Schema{},
			err:         assert.Error,
		},
		{
			description: "when storing valid schema, with an invalid version",
			toStore:     initMockSchema(12),
			lastSchema:  veritas.Schema{},
			err:         assert.Error,
		},
		{
			description: "when storing valid schema, with a valid initial version",
			toStore:     initMockSchema(0),
			lastSchema:  initMockSchema(1),
			schemas:     []veritas.Schema{initMockSchema(1)},
			err:         assert.NoError,
		},
		{
			description: "when updating a valid schema, with an invalid next version",
			toStore:     initMockSchema(12),
			lastSchema:  initMockSchema(1),
			schemas:     []veritas.Schema{initMockSchema(1)},
			err:         assert.Error,
		},
		{
			description: "when updating a valid schema, with a valid next version",
			toStore:     initMockSchema(1),
			lastSchema:  initMockSchema(2),
			schemas:     []veritas.Schema{initMockSchema(1), initMockSchema(2)},
			err:         assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			err := store.Store(ctx, collection, section, tc.toStore)

			tc.err(t, err, "should have returned the expected error assertion")

			fetched, err := store.SchemaFor(ctx, collection, section)
			if err != nil {
				t.Error(err)
			}

			tests.AssertSchemas(t, fetched, tc.lastSchema)
		})
	}
}
