package psql

/*
import (
	"context"
	"database/sql"
	"errors"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"

	gomega "github.com/onsi/gomega"
)

func TestInTransactionOnBeginFailure(t *testing.T) {
	gomega.RegisterTestingT(t)

	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fn := func(context.Context, pgx.Tx) error {
		return nil
	}

	mock.ExpectBegin().WillReturnError(errors.New("failed"))

	err = inTransaction(context.TODO(), db, fn)

	gomega.Expect(err).ToNot(gomega.BeNil(), "should return an error")
	gomega.Expect(mock.ExpectationsWereMet()).To(gomega.BeNil(), "All expectactions should be met")
}

func TestInTransactionOnHandlerSuccess(t *testing.T) {
	gomega.RegisterTestingT(t)

	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fn := func(context.Context, pgx.Tx) error {
		return nil
	}

	mock.ExpectBegin()
	mock.ExpectCommit()

	err = inTransaction(context.TODO(), db, fn)

	gomega.Expect(err).To(gomega.BeNil(), "should not return an error")
	gomega.Expect(mock.ExpectationsWereMet()).To(gomega.BeNil(), "All expectactions should be met")
}

func TestInTransactionOnHandlerSuccessCommitFails(t *testing.T) {
	gomega.RegisterTestingT(t)

	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fn := func(context.Context, pgx.Tx) error {
		return nil
	}

	mock.ExpectBegin()
	mock.ExpectCommit().WillReturnError(errors.New("failed"))

	err = inTransaction(context.TODO(), db, fn)

	gomega.Expect(err).ToNot(gomega.BeNil(), "should return an error")
	gomega.Expect(mock.ExpectationsWereMet()).To(gomega.BeNil(), "All expectactions should be met")
}

func TestInTransactionOnHandlerFaiiure(t *testing.T) {
	gomega.RegisterTestingT(t)

	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fn := func(context.Context, pgx.Tx) error {
		return errors.New("boom")
	}

	mock.ExpectBegin()
	mock.ExpectRollback()

	err = inTransaction(context.TODO(), db, fn)

	gomega.Expect(err).ToNot(gomega.BeNil(), "should not return an error")
	gomega.Expect(mock.ExpectationsWereMet()).To(gomega.BeNil(), "All expectactions should be met")
}

func TestInTransactionOnHandlerFaiiureRollbackFails(t *testing.T) {
	gomega.RegisterTestingT(t)

	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fn := func(context.Context, pgx.Tx) error {
		return errors.New("boom")
	}

	mock.ExpectBegin()
	mock.ExpectRollback().WillReturnError(errors.New("failed"))

	err = inTransaction(context.TODO(), db, fn)

	gomega.Expect(err).ToNot(gomega.BeNil(), "should return an error")
	gomega.Expect(mock.ExpectationsWereMet()).To(gomega.BeNil(), "All expectactions should be met")
}

func TestInTransactionOnHandlerPanic(t *testing.T) {
	gomega.RegisterTestingT(t)

	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fn := func(context.Context, pgx.Tx) error {
		panic("a")
	}

	mock.ExpectBegin()
	mock.ExpectRollback()

	err = inTransaction(context.TODO(), db, fn)

	gomega.Expect(err).ToNot(gomega.BeNil(), "should return an error")
	gomega.Expect(mock.ExpectationsWereMet()).To(gomega.BeNil(), "All expectactions should be met")
}
*/
