package psql

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/pkg/errors"
)

// TransactionHandler helper alias
type transactionHandler func(context.Context, *sql.Tx) error

// withTransaction helper function to process functions using a transaction
func withTransaction(ctx context.Context, tx *sql.Tx, handler transactionHandler) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = fmt.Errorf("%v", p)
		}

		if tx != nil {
			if err != nil {
				if rErr := tx.Rollback(); rErr != nil {
					err = errors.Wrap(rErr, err.Error())
				}
			} else {
				err = tx.Commit()
			}
		}
	}()

	if err = handler(ctx, tx); err != nil {
		return
	}

	return
}

// inTransaction helper function to process functions inside a database transaction
func inTransaction(ctx context.Context, pool *sql.DB, handler transactionHandler) error {
	tx, err := pool.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	return withTransaction(ctx, tx, handler)
}
