package psql

import (
	"context"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mandalore/veritas"
	"gitlab.com/mandalore/veritas/tests"
)

var (
	docKeyA = "d" + uuid.NewV4().String()
	docKeyB = "d" + uuid.NewV4().String()
	blkKeyA = "b" + uuid.NewV4().String()
	blkKeyB = "b" + uuid.NewV4().String()
)

type testBlockInSession struct {
	dID        string
	section    string
	bids       []string
	expectancy []veritas.BlockRecord
}

type testBlockForSession struct {
	dID        string
	sections   []string
	expectancy []veritas.BlockRecord
}

type testBlockInDocument struct {
	dID        string
	expectancy []veritas.BlockRecord
}

func TestSimpleStore(t *testing.T) {
	ctx := context.Background()

	pool, err := tests.GetTestPool("veritas")
	if err != nil {
		panic("failed to get test pq DB")
	}

	store, err := NewBlockStore(pool)
	if err != nil {
		t.Error(err)
	}

	testCases := []struct {
		description        string
		storables          []veritas.BlockRecord
		perBlockForSection []testBlockForSession
		perBlockInSection  []testBlockInSession
		perDocument        []testBlockInDocument
		storErr            assert.ErrorAssertionFunc
	}{
		{
			description: "when creating document",
			storErr:     assert.NoError,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyA,
					Version:       0,
					Collection:    "collection",
					DocumentID:    docKeyA,
					Section:       "section1",
					Payload:       []byte(`{"a":"b"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection: []testBlockInSession{
				{
					section: "section1",
					dID:     docKeyA,
					bids:    []string{blkKeyA},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"b"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perBlockForSection: []testBlockForSession{
				{
					sections: []string{"section1", "section2"},
					dID:      docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"b"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perDocument: []testBlockInDocument{
				{
					dID: docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"b"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
				{
					dID:        docKeyB,
					expectancy: nil,
				},
			},
		},
		{
			description: "when updating document",
			storErr:     assert.NoError,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyA,
					Version:       1,
					Collection:    "collection",
					DocumentID:    docKeyA,
					Section:       "section1",
					Payload:       []byte(`{"a":"c"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection: []testBlockInSession{
				{
					section: "section1",
					dID:     docKeyA,
					bids:    []string{blkKeyA},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 2, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perBlockForSection: []testBlockForSession{
				{
					sections: []string{"section1", "section2"},
					dID:      docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 2, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perDocument: []testBlockInDocument{
				{
					dID: docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 2, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
				{
					dID:        docKeyB,
					expectancy: nil,
				},
			},
		},
		{
			description: "when updating document and digest is the same", // MOTA FRACKED
			storErr:     assert.NoError,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyA,
					Version:       2,
					Collection:    "collection",
					DocumentID:    docKeyA,
					Section:       "section1",
					Digest:        []byte{1, 2},
					Payload:       []byte(`{"a":"c"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection: []testBlockInSession{
				{
					section: "section1",
					dID:     docKeyA,
					bids:    []string{blkKeyA},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
					},
				},
			},
			perBlockForSection: []testBlockForSession{
				{
					sections: []string{"section1", "section2"},
					dID:      docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
					},
				},
			},
		},
		{
			description: "when adding a second block on a second section",
			storErr:     assert.NoError,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyB,
					Version:       0,
					Collection:    "collection",
					DocumentID:    docKeyA,
					Section:       "section2",
					Payload:       []byte(`{"a":"d"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection: []testBlockInSession{
				{
					section: "section1",
					dID:     docKeyA,
					bids:    []string{blkKeyA},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
					},
				},
				{
					section: "section2",
					dID:     docKeyA,
					bids:    []string{blkKeyB},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyB, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section2", Payload: []byte(`{"a":"d"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perBlockForSection: []testBlockForSession{
				{
					sections: []string{"section1", "section2"},
					dID:      docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
						{ID: blkKeyB, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section2", Payload: []byte(`{"a":"d"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perDocument: []testBlockInDocument{
				{
					dID: docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
						{ID: blkKeyB, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section2", Payload: []byte(`{"a":"d"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
				{
					dID:        docKeyB,
					expectancy: nil,
				},
			},
		},
		{
			description: "when adding a new block on a new document",
			storErr:     assert.NoError,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyA,
					Version:       0,
					Collection:    "collection",
					DocumentID:    docKeyB,
					Section:       "section2",
					Payload:       []byte(`{"a":"f"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection: []testBlockInSession{
				{
					section: "section1",
					dID:     docKeyA,
					bids:    []string{blkKeyA},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
					},
				},
				{
					section: "section2",
					dID:     docKeyB,
					bids:    []string{blkKeyA},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 1, Collection: "collection", DocumentID: docKeyB, Section: "section2", Payload: []byte(`{"a":"f"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
				{
					section: "section2",
					dID:     docKeyA,
					bids:    []string{blkKeyB},
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyB, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section2", Payload: []byte(`{"a":"d"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perBlockForSection: []testBlockForSession{
				{
					sections: []string{"section1", "section2"},
					dID:      docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
						{ID: blkKeyB, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section2", Payload: []byte(`{"a":"d"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
			perDocument: []testBlockInDocument{
				{
					dID: docKeyA,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 3, Collection: "collection", DocumentID: docKeyA, Section: "section1", Payload: []byte(`{"a":"c"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{1, 2}},
						{ID: blkKeyB, Version: 1, Collection: "collection", DocumentID: docKeyA, Section: "section2", Payload: []byte(`{"a":"d"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
				{
					dID: docKeyB,
					expectancy: []veritas.BlockRecord{
						{ID: blkKeyA, Version: 1, Collection: "collection", DocumentID: docKeyB, Section: "section2", Payload: []byte(`{"a":"f"}`), Facts: []string{}, SchemaVersion: 12, Digest: []byte{}},
					},
				},
			},
		},
		{
			description: "when updating in the past",
			storErr:     assert.Error,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyA,
					Version:       1,
					Collection:    "collection",
					DocumentID:    docKeyA,
					Section:       "section1",
					Payload:       []byte(`{"a":"c"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection:  []testBlockInSession{},
			perBlockForSection: []testBlockForSession{},
		},
		{
			description: "when updating in the future",
			storErr:     assert.Error,
			storables: []veritas.BlockRecord{
				{
					ID:            blkKeyA,
					Version:       10000,
					Collection:    "collection",
					DocumentID:    docKeyA,
					Section:       "section1",
					Payload:       []byte(`{"a":"c"}`),
					Facts:         []string{},
					SchemaVersion: 12,
				},
			},
			perBlockInSection:  []testBlockInSession{},
			perBlockForSection: []testBlockForSession{},
		},
		{
			description: "when fetching missing blocks",
			storErr:     assert.Error,
			perBlockInSection: []testBlockInSession{
				{
					dID:     "zebedeu",
					section: "section1",
					bids:    []string{blkKeyA},
				},
				{
					dID:     docKeyA,
					section: "section11",
					bids:    []string{blkKeyA},
				},
				{
					dID:     docKeyA,
					section: "section1",
					bids:    []string{"nope"},
				},
			},
			perBlockForSection: []testBlockForSession{
				{
					dID:      "zebedeu",
					sections: []string{"section1", "section2"},
				},
				{
					dID:      docKeyA,
					sections: []string{"section10", "section20"},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			for _, storable := range tc.storables {
				err := store.StoreBlockRecords(ctx, storable)

				tc.storErr(t, err, "should have returned the expected error clause")
			}

			for _, r := range tc.perBlockForSection {
				recs, err := store.BlocksForSections(ctx, "collection", r.dID, r.sections)
				if err != nil {
					t.Error("should not have failed storing data", err)
				}

				assert.Equal(t, r.expectancy, recs, "should have recturned the expected elements")
			}

			for _, r := range tc.perBlockInSection {
				recs, err := store.BlocksInSection(ctx, "collection", r.dID, r.section, r.bids)
				if err != nil {
					t.Error("should not have failed storing data", err)
				}

				assert.ElementsMatch(t, r.expectancy, recs, "should have returned the correct blocks")
			}

			for _, r := range tc.perDocument {
				recs, err := store.BlocksForDocument(ctx, "collection", r.dID)
				if err != nil {
					t.Error("should not have failed storing data", err)
				}

				assert.Equal(t, r.expectancy, recs, "should have recturned the expected elements")
			}
		})
	}
}
