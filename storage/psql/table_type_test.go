package psql

import (
	"context"
	"testing"

	. "github.com/onsi/gomega"
	"gitlab.com/mandalore/veritas/tests"
)

func TestTableTypeAutoCreate(t *testing.T) {
	RegisterTestingT(t)

	var (
		ctx      = context.Background()
		id1, id2 int
		err      error
	)

	pool, err := tests.GetTestPool("veritas")
	if err != nil {
		panic("failed to get test pq DB")
	}

	tMap1 := NewTableTypeMap("tmap", WithIDCol("tmap_id"))
	tMap2 := NewTableTypeMap("tmap", WithIDCol("tmap_id"))

	id1, err = tMap1.IDFor(ctx, pool, "cenas1")
	Expect(err).To(BeNil(), "should not have returned an error")

	id2, err = tMap2.IDFor(ctx, pool, "cenas1")
	Expect(err).To(BeNil(), "should not have returned an error")

	Expect(id1).To(Equal(id2), "tMaps should be in sync")

	id2, err = tMap2.CreateIDFor(ctx, pool, "cenas2")
	Expect(err).To(BeNil(), "should not have returned an error")

	id1, err = tMap1.IDFor(ctx, pool, "cenas2")
	Expect(err).To(BeNil(), "should not have returned an error")

	Expect(id1).To(Equal(id2), "tMaps should be in sync")
}

func TestTableTypeWithNoAutoCreate(t *testing.T) {
	RegisterTestingT(t)

	var (
		ctx      = context.Background()
		id1, id2 int
		err      error
	)

	pool, err := tests.GetTestPool("veritas")
	if err != nil {
		panic("failed to get test pq DB")
	}

	tMap1 := NewTableTypeMap("tmap", WithIDCol("tmap_id"), WithNoAutoCreate())
	tMap2 := NewTableTypeMap("tmap", WithIDCol("tmap_id"), WithNoAutoCreate())

	_, err = tMap1.IDFor(ctx, pool, "cenas21")
	Expect(err).ToNot(BeNil(), "should have returned an error")

	_, err = tMap2.IDFor(ctx, pool, "cenas21")
	Expect(err).ToNot(BeNil(), "should have returned an error")

	id2, err = tMap2.CreateIDFor(ctx, pool, "cenas22")
	Expect(err).To(BeNil(), "should not have returned an error")

	id1, err = tMap1.IDFor(ctx, pool, "cenas22")
	Expect(err).To(BeNil(), "should not have returned an error")

	Expect(id1).To(Equal(id2), "tMaps should be in sync")
}
