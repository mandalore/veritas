package psql

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	lru "github.com/hashicorp/golang-lru"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/mandalore/veritas"
)

// MOTA : Add blob compression

type sval struct {
	slug string
	val  string
}

type blockKeyRequest struct {
	cFoID, sFoID, dFoID, bFoID string
}

type blockKey struct {
	cid, sid, did, bid int
}

type blockRow struct {
	k blockKey
	b veritas.BlockRecord
}

// BlockStore the postgres BlockStore implementation type
type BlockStore struct {
	pool        *sql.DB
	cache       *lru.Cache
	collections *TableTypeMap
	sections    *TableTypeMap
}

type NoOpCompressor struct{}

func (c NoOpCompressor) Compress(raw []byte) ([]byte, error) {
	raw = append(raw, 0)

	return raw, nil
}

func (c NoOpCompressor) Decompress(raw []byte) ([]byte, error) {
	return raw[0 : len(raw)-1], nil
}

// NewBlockStore factory method
func NewBlockStore(pool *sql.DB) (BlockStore, error) {
	cache, err := lru.New(1000) // Change This MOTA
	if err != nil {
		return BlockStore{}, err
	}

	return BlockStore{
		pool:        pool,
		cache:       cache,
		collections: NewTableTypeMap("collections", WithIDCol("collection_id")),
		sections:    NewTableTypeMap("sections", WithIDCol("section_id")),
	}, nil
}

//BlocksForDocument fetches all blocks for a particular document
func (s BlockStore) BlocksForDocument(ctx context.Context, collection string, dID string) ([]veritas.BlockRecord, error) {
	var reqs = []blockKeyRequest{
		{cFoID: collection, dFoID: dID},
	}

	if len(reqs) == 0 {
		return nil, nil
	}

	bkys, err := s.buildBlockKeys(ctx, reqs, false)
	if err != nil {
		return nil, fmt.Errorf("failed to generate blockKeys: %w", err)
	}

	return s.getByBlockKeys(ctx, bkys)
}

//BlocksInSection fetches blocks for a section on a document on a particular collection
func (s BlockStore) BlocksInSection(ctx context.Context, collection string, dID string, section string, bIDs []string) ([]veritas.BlockRecord, error) {
	var reqs = make([]blockKeyRequest, 0, len(bIDs))

	for _, bID := range bIDs {
		reqs = append(reqs, blockKeyRequest{cFoID: collection, dFoID: dID, sFoID: section, bFoID: bID})
	}

	if len(reqs) == 0 {
		return nil, nil
	}

	bkys, err := s.buildBlockKeys(ctx, reqs, false)
	if err != nil {
		return nil, fmt.Errorf("failed to generate blockKeys: %w", err)
	}

	for _, bky := range bkys {
		if bky.bid == 0 {
			return nil, nil
		}
	}

	return s.getByBlockKeys(ctx, bkys)
}

// BlocksForSections fetches blocks for a collection, document, and with filtered sections
func (s BlockStore) BlocksForSections(ctx context.Context, collection string, dID string, sections []string) ([]veritas.BlockRecord, error) {
	var reqs = make([]blockKeyRequest, 0, len(sections))

	for _, section := range sections {
		reqs = append(reqs, blockKeyRequest{cFoID: collection, dFoID: dID, sFoID: section})
	}

	if len(reqs) == 0 {
		return nil, nil
	}

	bkys, err := s.buildBlockKeys(ctx, reqs, false)
	if err != nil {
		return nil, fmt.Errorf("failed to generate blockKeys: %w", err)
	}

	return s.getByBlockKeys(ctx, bkys)
}

func (s BlockStore) getByBlockKeys(ctx context.Context, bkys []blockKey) ([]veritas.BlockRecord, error) {
	var (
		recs     []veritas.BlockRecord
		qArgs    = []interface{}{}
		qBuilder strings.Builder
	)

	qBuilder.WriteString(`
		SELECT c.slug as collection, b.document_fo_id, s.slug as section, b.block_fo_id, schema_id, digest, data, facts, version
		FROM blocks b
		JOIN collections c ON (b.collection_id = c.collection_id)
		JOIN sections s ON (b.section_id = s.section_id)
		WHERE 
	`)

	var pos = 1

	for i, bky := range bkys {
		if bky.cid == 0 {
			return recs, errors.New("you must specify a collection id")
		}

		qBuilder.WriteString("( b.collection_id = $")
		qBuilder.WriteString(strconv.Itoa(pos))
		qBuilder.WriteString("::INT")
		qArgs = append(qArgs, bky.cid)
		pos++

		qBuilder.WriteString(" AND b.document_id = $")
		qBuilder.WriteString(strconv.Itoa(pos))
		qBuilder.WriteString("::INT")
		qArgs = append(qArgs, bky.did)
		pos++

		if bky.sid != 0 {
			qBuilder.WriteString(" AND b.section_id = $")
			qBuilder.WriteString(strconv.Itoa(pos))
			qBuilder.WriteString("::INT")
			qArgs = append(qArgs, bky.sid)
			pos++
		}

		if bky.bid != 0 {
			qBuilder.WriteString(" AND b.block_id = $")
			qBuilder.WriteString(strconv.Itoa(pos))
			qBuilder.WriteString("::INT")
			qArgs = append(qArgs, bky.bid)
			pos++
		}

		qBuilder.WriteRune(')')

		if i < len(bkys)-1 {
			qBuilder.WriteString(" OR ")
		}
	}

	rows, err := s.pool.QueryContext(ctx, qBuilder.String(), qArgs...)
	if err != nil {
		return recs, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			rec veritas.BlockRecord
			raw []byte
			err error
		)

		if err := rows.Scan(&rec.Collection, &rec.DocumentID, &rec.Section, &rec.ID, &rec.SchemaVersion, &rec.Digest, &raw, pq.Array(&rec.Facts), &rec.Version); err != nil {
			return recs, err
		}

		rec.Payload, err = NoOpCompressor{}.Decompress(raw)
		if err != nil {
			return recs, err
		}

		recs = append(recs, rec)
	}

	if err := rows.Err(); err != nil {
		return recs, err
	}

	return recs, nil
}

// StoreBlockRecords implements the BlockStore interface
func (s BlockStore) StoreBlockRecords(ctx context.Context, records ...veritas.BlockRecord) error {
	if len(records) == 0 {
		return nil
	}

	bRows, err := s.buildBlockRows(ctx, records)
	if err != nil {
		return fmt.Errorf("failed to generate blockKeys: %w", err)
	}

	return inTransaction(ctx, s.pool, func(ctx context.Context, tx *sql.Tx) error {
		for _, bRow := range bRows {
			if err := s.storeBlockRows(ctx, tx, bRow); err != nil {
				return fmt.Errorf("failed to store block: %w", err)
			}
		}

		return nil
	})
}

func (s BlockStore) cachedIDFor(grpID int, foID string) (int, bool) {
	v, ok := s.cache.Get(fmt.Sprintf("%d::%s", grpID, foID))
	if ok {
		id, ok := v.(int)
		if ok {
			return id, true
		}
	}

	return 0, false
}

func (s BlockStore) buildBlockKeys(ctx context.Context, reqs []blockKeyRequest, autoCreate bool) ([]blockKey, error) {
	var (
		bKeys  = make([]blockKey, 0, len(reqs))
		misses = map[string]bool{}
	)

	for _, r := range reqs {
		var bID, sID int

		cID, err := s.collections.IDFor(ctx, s.pool, r.cFoID)
		if err != nil {
			return nil, fmt.Errorf("failed to get collection %s: %w", r.cFoID, err)
		}

		dID, ok := s.cachedIDFor(cID, r.dFoID)
		if !ok {
			misses[fmt.Sprintf("%d::%s", cID, r.dFoID)] = true
		}

		if r.sFoID != "" {
			sID, err = s.sections.IDFor(ctx, s.pool, r.sFoID)
			if err != nil {
				return nil, fmt.Errorf("failed to get section %s: %w", r.sFoID, err)
			}
		}

		if r.bFoID != "" {
			bID, ok = s.cachedIDFor(sID, r.bFoID)
			if !ok {
				misses[fmt.Sprintf("%d::%s", sID, r.bFoID)] = true
			}
		}

		bKeys = append(bKeys, blockKey{cid: cID, did: dID, sid: sID, bid: bID})
	}

	if len(misses) != 0 {
		ids, err := s.idsFor(ctx, misses, autoCreate)
		if err != nil {
			return nil, fmt.Errorf("failed to generate new ids: %w", err)
		}

		for i := range bKeys {
			if bKeys[i].did == 0 {
				bKeys[i].did = ids[fmt.Sprintf("%d::%s", bKeys[i].cid, reqs[i].dFoID)]
			}

			if bKeys[i].bid == 0 && reqs[i].bFoID != "" {
				bKeys[i].bid = ids[fmt.Sprintf("%d::%s", bKeys[i].sid, reqs[i].bFoID)]
			}
		}
	}

	return bKeys, nil
}

func (s BlockStore) idsFor(ctx context.Context, misses map[string]bool, autoCreate bool) (map[string]int, error) {
	if autoCreate {
		return s.generateIDsFor(ctx, misses)
	}

	return s.fetchIDsFor(ctx, misses)
}

func (s BlockStore) buildBlockRows(ctx context.Context, records []veritas.BlockRecord) ([]blockRow, error) {
	var (
		reqs  = make([]blockKeyRequest, 0, len(records))
		bRows = make([]blockRow, 0, len(records))
	)

	for _, r := range records {
		reqs = append(reqs, blockKeyRequest{cFoID: r.Collection, dFoID: r.DocumentID, sFoID: r.Section, bFoID: r.ID})
	}

	bkeys, err := s.buildBlockKeys(ctx, reqs, true)
	if err != nil {
		return nil, fmt.Errorf("failed to generate blockKeys: %w", err)
	}

	for i := range records {
		bRows = append(bRows, blockRow{k: bkeys[i], b: records[i]})
	}

	return bRows, nil
}

func (s BlockStore) fetchIDsFor(ctx context.Context, misses map[string]bool) (map[string]int, error) {
	var (
		qBuilder strings.Builder
		idx      int
		res      = make(map[string]int, len(misses))
		qArgs    = make([]interface{}, 0, len(misses)*2)
	)

	qBuilder.WriteString(`WITH vals("fo_group_id", "fo_id") AS ( VALUES `)

	for k := range misses {
		qBuilder.WriteString(fmt.Sprintf("( $%d::INT, $%d )", idx*2+1, idx*2+2))
		idx++

		if idx != len(misses) {
			qBuilder.WriteRune(',')
		}

		for _, col := range strings.Split(k, "::") {
			qArgs = append(qArgs, col)
		}
	}

	qBuilder.WriteString(`), cv AS (
		SELECT vals.fo_group_id, vals.fo_id, fo_lookup.unique_id
		FROM vals, fo_lookup
		WHERE fo_lookup.fo_group_id = vals.fo_group_id AND fo_lookup.fo_id = vals.fo_id
	)
	SELECT * FROM cv WHERE cv.unique_id IS NOT NULL`)

	rows, err := s.pool.QueryContext(ctx, qBuilder.String(), qArgs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		groupID, uID int
		foID         string
	)

	for rows.Next() {
		if err := rows.Scan(&groupID, &foID, &uID); err != nil {
			return res, err
		}

		k := fmt.Sprintf("%d::%s", groupID, foID)
		s.cache.Add(k, uID)

		res[k] = uID
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return res, nil
}

func (s BlockStore) generateIDsFor(ctx context.Context, misses map[string]bool) (map[string]int, error) {
	var (
		qBuilder strings.Builder
		idx      int
		res      = make(map[string]int, len(misses))
		qArgs    = make([]interface{}, 0, len(misses)*2)
	)

	qBuilder.WriteString(`WITH vals("fo_group_id", "fo_id") AS ( VALUES `)

	for k := range misses {
		qBuilder.WriteString(fmt.Sprintf("( $%d::INT, $%d )", idx*2+1, idx*2+2))
		idx++

		if idx != len(misses) {
			qBuilder.WriteRune(',')
		}

		for _, col := range strings.Split(k, "::") {
			qArgs = append(qArgs, col)
		}
	}

	qBuilder.WriteString(`), cv AS (
		SELECT vals.fo_group_id, vals.fo_id, fo_lookup.unique_id
		FROM vals, fo_lookup
		WHERE fo_lookup.fo_group_id = vals.fo_group_id AND fo_lookup.fo_id = vals.fo_id
	), nv AS (
		INSERT INTO fo_lookup ("fo_group_id", "fo_id", "unique_id")
		SELECT vals.fo_group_id, vals.fo_id, nextval('fo_lookup_unique_id_seq') as unique_id
		FROM vals
		LEFT JOIN cv ON (vals.fo_group_id = cv.fo_group_id AND vals.fo_id = cv.fo_id)
		WHERE cv.unique_id IS NULL
		ON CONFLICT("fo_group_id", "fo_id") DO NOTHING
		RETURNING *
	)
	SELECT * FROM nv
	UNION ALL
	SELECT * FROM cv WHERE cv.unique_id IS NOT NULL`)

	rows, err := s.pool.QueryContext(ctx, qBuilder.String(), qArgs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		groupID, uID int
		foID         string
	)

	for rows.Next() {
		if err := rows.Scan(&groupID, &foID, &uID); err != nil {
			return res, err
		}

		k := fmt.Sprintf("%d::%s", groupID, foID)
		s.cache.Add(k, uID)

		res[k] = uID
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return res, nil
}

func (s BlockStore) storeBlockRows(ctx context.Context, tx *sql.Tx, bRow blockRow) error {
	payload, err := NoOpCompressor{}.Compress(bRow.b.Payload)
	if err != nil {
		return fmt.Errorf("failed to compress block: %w", err)
	}

	res, err := tx.ExecContext(
		ctx, `INSERT INTO blocks
(collection_id, document_id, section_id, block_id, schema_id, digest, data, facts, version, document_fo_id, block_fo_id)
VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
ON CONFLICT (collection_id, document_id, section_id, block_id) DO UPDATE SET
schema_id = EXCLUDED.schema_id,
digest    = EXCLUDED.digest,
data      = EXCLUDED.data,
facts     = EXCLUDED.facts,
version   = EXCLUDED.version
WHERE blocks.Version = $9 - 1`,
		bRow.k.cid,
		bRow.k.did,
		bRow.k.sid,
		bRow.k.bid,
		bRow.b.SchemaVersion,
		bRow.b.Digest,
		payload,
		pq.Array(bRow.b.Facts),
		bRow.b.Version+1,
		bRow.b.DocumentID,
		bRow.b.ID,
	)
	if err != nil {
		return fmt.Errorf("failure storing block %s/%s/%s/%s: %w", bRow.b.Collection, bRow.b.DocumentID, bRow.b.Section, bRow.b.ID, err)
	}

	rc, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("failure storing block %s/%s/%s/%s: %w", bRow.b.Collection, bRow.b.DocumentID, bRow.b.Section, bRow.b.ID, err)
	}

	if rc != 1 {
		return fmt.Errorf("conflict storing block %s/%s/%s/%s: %d,%d", bRow.b.Collection, bRow.b.DocumentID, bRow.b.Section, bRow.b.ID, bRow.k.bid, bRow.b.Version)
	}

	return nil
}

/*
func (s BlockStore) storeIndexTuples(ctx context.Context, tx *sql.Tx, collectionID int, key []byte, tups []veritas.IndexTuple) error {
	var (
		new, cur  = map[string]sval{}, map[string]sval{}
		slug, val string
	)

	rows, err := tx.Query(ctx, `
		SELECT slug, val
		FROM skeys
		WHERE collection_id = $1 AND document_key = $2
	`, collectionID, key)
	if err != nil {
		return fmt.Errorf("failed to fetch skeys: %w", err)
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&slug, &val); err != nil {
			return fmt.Errorf("failed to scan skeys: %w", err)
		}
		cur[slug+"::"+val] = sval{slug, val}
	}
	if rows.Err() != nil {
		return fmt.Errorf("failed to iterate skeys: %w", rows.Err())
	}

	for _, tup := range tups {
		for _, val := range tup.Vals {
			new[tup.Slug+"::"+val] = sval{tup.Slug, val}
		}
	}

	for k, v := range cur {
		if _, ok := new[k]; ok {
			continue
		}

		_, err := tx.Exec(ctx, `DELETE FROM skeys WHERE collection_id = $1 AND slug = $2 AND val = $3`, collectionID, v.slug, v.val)
		if err != nil {
			return err
		}
	}

	for k, v := range new {
		if _, ok := cur[k]; ok {
			continue
		}

		_, err := tx.Exec(ctx, `INSERT INTO skeys (collection_id, slug, val, document_key) VALUES($1,$2,$3,$4)`, collectionID, v.slug, v.val, key)
		if err != nil {
			return err
		}
	}

	return nil
}
*/

/*
// GetBySecondaryKey fetches a Document using on of it's Secondary Keys
func (s BlockStore) GetBySecondaryKey(ctx context.Context, collection, slug string, val string) (veritas.Document, error) {
	collectionID, err := s.collections.IDFor(s.pool, collection)
	if err != nil {
		return veritas.Document{}, fmt.Errorf("failed to get collection %s: %w", collection, err)
	}

	key, err := s.getKeyFor(ctx, collectionID, slug, val)
	if err != nil {
		return veritas.Document{}, fmt.Errorf("failed to get document %s/%s/%s: %w", collection, slug, val, err)
	}

	return s.getByKey(ctx, collectionID, key)
}

func (s BlockStore) getKeyFor(ctx context.Context, collectionID int, slug string, val string) ([]byte, error) {
	var key []byte

	row := s.pool.QueryRow(ctx, `
		SELECT document_key
		FROM skeys d
		WHERE collection_id = $1 AND slug = $2 AND val = $3
	`, collectionID, slug, val)

	if err := row.Scan(&key); err != nil {
		return nil, err
	}

	return key, nil
}


*/
