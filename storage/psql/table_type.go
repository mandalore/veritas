package psql

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

// Errors
var (
	ErrMappingNotFound = errors.New("slug not found")
)

type Querier interface {
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
}

// TableTypeMap ...
type TableTypeMap struct {
	mu         sync.RWMutex
	table      string
	idcol      string
	cache      map[string]int
	autoCreate bool
	loaded     bool
}

// WithOption ... Functional Option Type Definition
type WithOption func(*TableTypeMap)

// NewTableTypeMap ...
func NewTableTypeMap(table string, options ...WithOption) *TableTypeMap {
	tmap := &TableTypeMap{
		table:      table,
		idcol:      strings.TrimSuffix(table, "s") + "_id",
		cache:      make(map[string]int),
		autoCreate: true,
	}

	for _, option := range options {
		option(tmap)
	}

	return tmap
}

// WithIDCol Method to implement option to set the ID Col for this map
func WithIDCol(idcol string) WithOption {
	return func(tmap *TableTypeMap) {
		tmap.idcol = idcol
	}
}

// WithNoAutoCreate Disables auto create on not found
func WithNoAutoCreate() WithOption {
	return func(tmap *TableTypeMap) {
		tmap.autoCreate = false
	}
}

// IDFor ...
func (tmap *TableTypeMap) IDFor(ctx context.Context, q Querier, slug string) (int, error) {
	if err := tmap.loadCache(ctx, q); err != nil {
		return 0, err
	}

	typeID, found := tmap.idFor(q, slug)
	if found {
		return typeID, nil
	}

	if tmap.autoCreate {
		id, err := tmap.CreateIDFor(ctx, q, slug)
		if err != nil {
			return 0, ErrMappingNotFound
		}

		return id, nil
	}

	id, err := tmap.refreshCacheWith(ctx, q, slug)
	if err != nil {
		return 0, ErrMappingNotFound
	}

	return id, nil
}

func (tmap *TableTypeMap) idFor(q Querier, slug string) (int, bool) {
	tmap.mu.RLock()
	defer tmap.mu.RUnlock()

	id, found := tmap.cache[slug]
	return id, found
}

// CreateIDFor ...
func (tmap *TableTypeMap) CreateIDFor(ctx context.Context, q Querier, slug string) (int, error) {
	tmap.mu.Lock()
	defer tmap.mu.Unlock()

	var id int

	row := q.QueryRowContext(
		ctx,
		fmt.Sprintf(`INSERT INTO %s (slug) VALUES ($1) RETURNING (%s)`, tmap.table, tmap.idcol),
		slug,
	)
	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	tmap.loaded = false

	return id, nil
}

func (tmap *TableTypeMap) refreshCacheWith(ctx context.Context, q Querier, slug string) (int, error) {
	tmap.mu.Lock()
	defer tmap.mu.Unlock()

	var id int

	row := q.QueryRowContext(ctx, fmt.Sprintf(`SELECT %s FROM %s WHERE slug = $1`, tmap.idcol, tmap.table), slug)
	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	tmap.cache[slug] = id

	return id, nil
}

func (tmap *TableTypeMap) loadCache(ctx context.Context, q Querier) error {
	tmap.mu.Lock()
	defer tmap.mu.Unlock()

	if tmap.loaded {
		return nil
	}

	rows, err := q.QueryContext(ctx, fmt.Sprintf(`SELECT %s, slug FROM %s`, tmap.idcol, tmap.table))
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id   int
			slug string
		)

		if err := rows.Scan(&id, &slug); err != nil {
			return err
		}

		tmap.cache[slug] = id
	}

	if err := rows.Err(); err != nil {
		return err
	}

	tmap.loaded = true

	return nil
}
