package cached

/*
import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"

	"gitlab.com/mandalore/veritas"
)

const (
	lruSize = 256
)

const (
	stateReady = iota
	stateInconsistent
)

type listener interface {
	Listen(ctx context.Context) (chan veritas.Notification, error)
}

type store interface {
	Store(ctx context.Context, schema veritas.Schema) error
	Schema(ctx context.Context, collection string) (veritas.Schema, error)
	Schemas(ctx context.Context, collection string) ([]veritas.Schema, error)
}

type cache interface {
	Set(ctx context.Context, k, v interface{}) error
	Get(ctx context.Context, k interface{}) (interface{}, error)
	Del(ctx context.Context, k interface{}) error
}

// SchemaStore the postgres SchemaStore implementation type
type SchemaStore struct {
	store    store
	cache    cache
	listener listener
	lock     *sync.RWMutex
	cancel   context.CancelFunc
	state    int32
}

// NewSchemaStore factory method
func NewSchemaStore(st store, cx cache, listener listener) (*SchemaStore, error) {
	ctx, cancel := context.WithCancel(context.Background())

	s := SchemaStore{
		store:    st,
		cache:    cx,
		listener: listener,
		lock:     &sync.RWMutex{},
		cancel:   cancel,
		state:    stateReady,
	}

	go s.listen(ctx)

	return &s, nil
}

// Stop cleanly cancels service
func (s *SchemaStore) Stop() error {
	if s.cancel == nil {
		return fmt.Errorf("cannot stop")
	}

	s.cancel()

	return nil
}

func (s *SchemaStore) listen(ctx context.Context) {
	q, err := s.listener.Listen(ctx)
	if err != nil {
		s.handleListenFailure(ctx, err)
	}

	for {
		select {
		case <-ctx.Done():
			s.handleListenFailure(ctx, fmt.Errorf("listener was closed"))
			return
		case m, ok := <-q:
			if !ok {
				s.handleListenFailure(ctx, fmt.Errorf("channel was closed"))
				return
			}
			if err := s.handleListenFailure(ctx, m.Err); err != nil {
				return
			}

			if m.Key != nil {
				err := s.cache.Del(ctx, m.Key)
				if err := s.handleListenFailure(ctx, err); err != nil {
					return
				}
			}
		}
	}
}

func (s *SchemaStore) handleListenFailure(ctx context.Context, err error) error {
	if err != nil {
		atomic.StoreInt32(&s.state, stateInconsistent)
	}

	return err
}

func (s *SchemaStore) isConsistent() bool {
	return atomic.LoadInt32(&s.state) == stateReady
}

// Store stores a new schema version
func (s *SchemaStore) Store(ctx context.Context, schema veritas.Schema) error {
	return s.store.Store(ctx, schema)
}

// Schemas returns all known Schemas for a particular collection
func (s *SchemaStore) Schemas(ctx context.Context, collection string) ([]veritas.Schema, error) {
	return s.store.Schemas(ctx, collection)
}

// Schema returns the latest Schema for a particular collection
func (s *SchemaStore) Schema(ctx context.Context, collection string) (veritas.Schema, error) {
	if !s.isConsistent() {
		return veritas.Schema{}, fmt.Errorf("inconsistent storage")
	}

	cur, err := s.cache.Get(ctx, collection)
	if err != nil {
		return veritas.Schema{}, err
	}

	if cur != nil {
		val, ok := cur.(veritas.Schema)
		if !ok {
			return veritas.Schema{}, fmt.Errorf("failed to hydrate cached value")
		}

		return val, nil
	}

	return s.getAndSet(ctx, collection)
}

func (s SchemaStore) getAndSet(ctx context.Context, collection string) (veritas.Schema, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	new, err := s.store.Schema(ctx, collection)
	if err != nil {
		return veritas.Schema{}, fmt.Errorf("failed to fetch value from cache")
	}

	s.cache.Set(ctx, collection, new)

	return new, nil
}
*/
