package cached

/*
import (
	context "context"
	"fmt"
	"testing"
	"time"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	veritas "gitlab.com/mandalore/veritas"
)

func TestStore(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)

	testCases := []struct {
		description string
		store       func() *Mockstore
		cache       func() *Mockcache
		listener    func() *Mocklistener
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when durable store returns an error",
			store: func() *Mockstore {
				mock := NewMockstore(ctrl)

				mock.EXPECT().Store(ctx, veritas.Schema{}).Return(fmt.Errorf("failed"))

				return mock
			},
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			cache: func() *Mockcache { return NewMockcache(ctrl) },
			err:   assert.Error,
		},
		{
			description: "when durable store succeeds",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				mock := NewMockstore(ctrl)

				mock.EXPECT().Store(ctx, veritas.Schema{}).Return(nil)

				return mock
			},
			cache: func() *Mockcache { return NewMockcache(ctrl) },
			err:   assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			cstore, err := NewSchemaStore(tc.store(), tc.cache(), tc.listener())
			if err != nil {
				t.Error(err)
			}
			defer cstore.Stop()

			err = cstore.Store(ctx, veritas.Schema{})

			tc.err(t, err, "expected error assertion verified")
		})
	}
}

func TestSchemas(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)

	testCases := []struct {
		description string
		store       func() *Mockstore
		cache       func() *Mockcache
		listener    func() *Mocklistener
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when durable store returns an error",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				mock := NewMockstore(ctrl)

				mock.EXPECT().Schemas(ctx, "cenas").Return(nil, fmt.Errorf("failed"))

				return mock
			},
			cache: func() *Mockcache { return NewMockcache(ctrl) },
			err:   assert.Error,
		},
		{
			description: "when durable store succeeds",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				mock := NewMockstore(ctrl)

				mock.EXPECT().Schemas(ctx, "cenas").Return([]veritas.Schema{
					veritas.Schema{},
					veritas.Schema{},
				}, nil)

				return mock
			},
			cache: func() *Mockcache { return NewMockcache(ctrl) },
			err:   assert.NoError,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			cstore, err := NewSchemaStore(tc.store(), tc.cache(), tc.listener())
			if err != nil {
				t.Error(err)
			}
			defer cstore.Stop()

			_, err = cstore.Schemas(ctx, "cenas")

			tc.err(t, err, "expected error assertion verified")
		})
	}
}

func TestSchema(t *testing.T) {
	var (
		ctx    = context.Background()
		ctrl   = gomock.NewController(t)
		schema = veritas.Schema{Collection: "cenas", Version: 12}
	)

	testCases := []struct {
		description string
		store       func() *Mockstore
		cache       func() *Mockcache
		expected    veritas.Schema
		listener    func() *Mocklistener
		failed      bool
		err         assert.ErrorAssertionFunc
	}{

		{
			description: "when cache store returns an error",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				return NewMockstore(ctrl)
			},
			cache: func() *Mockcache {
				mock := NewMockcache(ctrl)

				mock.EXPECT().Get(ctx, "cenas").Return(nil, fmt.Errorf("failed"))

				return mock
			},
			err:      assert.Error,
			expected: veritas.Schema{},
		},
		{
			description: "when cache store does not have the elemant cached, durable fails to fetch",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				mock := NewMockstore(ctrl)

				mock.EXPECT().Schema(ctx, "cenas").Return(veritas.Schema{}, fmt.Errorf("failed"))

				return mock
			},
			cache: func() *Mockcache {
				mock := NewMockcache(ctrl)

				mock.EXPECT().Get(ctx, "cenas").Return(nil, nil)

				return mock
			},
			err:      assert.Error,
			expected: veritas.Schema{},
		},
		{
			description: "when cache store does not have the elemant cached, fails to store in cache",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				mock := NewMockstore(ctrl)

				mock.EXPECT().Schema(ctx, "cenas").Return(schema, nil)

				return mock
			},
			cache: func() *Mockcache {
				mock := NewMockcache(ctrl)

				mock.EXPECT().Get(ctx, "cenas").Return(nil, nil)
				mock.EXPECT().Set(ctx, "cenas", schema).Return(fmt.Errorf("failed"))

				return mock
			},
			err:      assert.NoError,
			expected: schema,
		},
		{
			description: "when cache store has the element cached",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store: func() *Mockstore {
				return NewMockstore(ctrl)
			},
			cache: func() *Mockcache {
				mock := NewMockcache(ctrl)

				mock.EXPECT().Get(ctx, "cenas").Return(schema, nil)

				return mock
			},
			err:      assert.NoError,
			expected: schema,
		},
		{
			description: "when cache store has the element cached, but state is inconsistent",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), nil)

				return listener
			},
			store:    func() *Mockstore { return NewMockstore(ctrl) },
			cache:    func() *Mockcache { return NewMockcache(ctrl) },
			failed:   true,
			err:      assert.Error,
			expected: veritas.Schema{},
		},
		{
			description: "when listener listen fails",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				listener.EXPECT().Listen(gomock.Any()).Return(make(chan veritas.Notification), fmt.Errorf("boom"))

				return listener
			},
			store:    func() *Mockstore { return NewMockstore(ctrl) },
			cache:    func() *Mockcache { return NewMockcache(ctrl) },
			err:      assert.Error,
			expected: veritas.Schema{},
		},
		{
			description: "when listener makes storage inconsistent on listener failure",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				ch := make(chan veritas.Notification, 1)
				ch <- veritas.Notification{Key: nil, Err: fmt.Errorf("zee bang")}

				listener.EXPECT().Listen(gomock.Any()).Return(ch, nil)

				return listener
			},
			store:    func() *Mockstore { return NewMockstore(ctrl) },
			cache:    func() *Mockcache { return NewMockcache(ctrl) },
			err:      assert.Error,
			expected: veritas.Schema{},
		},
		{
			description: "when cache bust storage inconsistent",
			listener: func() *Mocklistener {
				listener := NewMocklistener(ctrl)

				ch := make(chan veritas.Notification, 1)
				ch <- veritas.Notification{Key: "zelito", Err: nil}

				listener.EXPECT().Listen(gomock.Any()).Return(ch, nil)

				return listener
			},
			store: func() *Mockstore { return NewMockstore(ctrl) },
			cache: func() *Mockcache {
				cache := NewMockcache(ctrl)

				cache.EXPECT().Del(gomock.Any(), "zelito").Return(fmt.Errorf("failed"))

				return cache
			},
			err:      assert.Error,
			expected: veritas.Schema{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			cstore, err := NewSchemaStore(tc.store(), tc.cache(), tc.listener())
			if err != nil {
				t.Error(err)
			}
			defer cstore.Stop()

			if tc.failed {
				cstore.handleListenFailure(ctx, fmt.Errorf("break me"))
			}

			time.Sleep(100 * time.Millisecond)

			actual, err := cstore.Schema(ctx, "cenas")
			tc.err(t, err, "expected error assertion verified")
			assert.Equal(t, tc.expected, actual, "returned and expected values match")
		})
	}
}


cache, err := x.NewCache(
	lruSize, q, func(ctx context.Context, k interface{}) (interface{}, error) {
		collection, ok := k.(string)
		if !ok {
			return nil, fmt.Errorf("failed to supply a collection string")
		}

		return schemaFromDatabase(ctx, pool, colMap, collection)
	},
)

	listener := x.NewListener(pool, "schemas")
	q, err := listener.Listen(context.Background())
	if err != nil {
		return SchemaStore{}, err
	}
*/
