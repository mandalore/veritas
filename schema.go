package veritas

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/tidwall/gjson"
	"gitlab.com/mandalore/veritas/validators"
)

type defaultDigester struct{}

func (d defaultDigester) Digest(payload []byte) ([]byte, error) {
	sum := sha256.Sum256(payload)

	return sum[:], nil
}

type validator interface {
	Validate(payload []byte) error
}

// Validator defines a common Validator Envelope
type Validator struct {
	typ    string
	def    []byte
	driver validator
}

// NewValidator factory method to create a Validator
func NewValidator(typ string, def []byte) (Validator, error) {
	var (
		driver validator
		err    error
	)

	switch typ {
	case "avro":
		driver, err = validators.NewAvroValidator(def)
	case "json":
		driver, err = validators.NewJSONSchemaValidator(def)
	case "acceptall":
		driver = validators.AcceptAll{}
	case "denyall":
		driver = validators.DenyAll{}
	default:
		err = ErrInvalidValidator
	}

	if err != nil {
		return Validator{}, err
	}

	return Validator{
		typ:    typ,
		def:    def,
		driver: driver,
	}, nil
}

// IsValid check if validator is Valid
func (v Validator) IsValid() error {
	if v.typ == "" {
		return ErrInvalidValidator
	}

	if v.driver == nil {
		return ErrInvalidValidator
	}

	return nil
}

// Validate ...
func (v Validator) Validate(p []byte) error {
	if err := v.IsValid(); err != nil {
		return err
	}

	return v.driver.Validate(p)
}

// UnmarshalJSON implements unmarshal JSON
func (v *Validator) UnmarshalJSON(b []byte) error {
	typ := gjson.GetBytes(b, "typ").String()
	def := gjson.GetBytes(b, "def").String()

	t, err := NewValidator(typ, []byte(def))
	if err != nil {
		return err
	}

	v.typ = t.typ
	v.def = t.def
	v.driver = t.driver

	return nil
}

// MarshalJSON ...
func (v Validator) MarshalJSON() ([]byte, error) {
	var t = struct {
		Typ string          `json:"typ"`
		Def json.RawMessage `json:"def"`
	}{
		Typ: v.typ,
		Def: v.def,
	}

	return json.Marshal(&t)
}

// Digester interface defining Digest functions
type Digester interface {
	Digest(payload []byte) ([]byte, error)
}

// Mapper interface defining structs with the ability to Map Documents
type Mapper interface {
	Map(b Block) (map[string]interface{}, error)
}

// Index type for index definitions
type Index struct {
	Name   string
	Unique bool
	Path   string
	Fields []string
}

// Schema type defining a Schema Definition
// Digester and mappings don't allow serialization yet
type Schema struct {
	Version   uint32
	validator Validator
	indexes   []Index
	digester  Digester
	mappings  map[string]Mapper
}

// SchemaOptional type alias for first order function that add optional attributtes to a Schema
type SchemaOptional func(d *Schema) error

// WithIndexes first order function to use as an optional parameter for the NewSchema factory method
// this function adds option Indexes
func WithIndexes(idxs ...Index) SchemaOptional {
	return func(s *Schema) error {
		s.indexes = idxs

		return nil
	}
}

// WithDigester first order function to use as an optional parameter for the NewSchema factory method
// this function adds an optional Version
func WithDigester(d Digester) SchemaOptional {
	return func(s *Schema) error {
		if d == nil {
			return ErrInvalidDigester
		}

		s.digester = d

		return nil
	}
}

// WithMapper first order function to use as an optional parameter for the NewSchema factory method
// this function adds an optional Mapper
func WithMapper(slug string, m Mapper) SchemaOptional {
	return func(s *Schema) error {
		if slug == "" || m == nil {
			return ErrInvalidMapper
		}

		s.mappings[slug] = m

		return nil
	}
}

// NewSchema factory method used to create a NewSchema
func NewSchema(v Validator, opts ...SchemaOptional) (Schema, error) {
	s := Schema{
		validator: v,
		indexes:   []Index{},
		digester:  defaultDigester{},
		mappings:  map[string]Mapper{},
	}

	if err := s.IsValid(); err != nil {
		return s, err
	}

	for _, opt := range opts {
		if err := opt(&s); err != nil {
			return s, err
		}
	}

	return s, nil
}

// IsValid validates if the current schema is valid
func (s Schema) IsValid() error {
	if err := s.validator.IsValid(); err != nil {
		return ErrInvalidValidator
	}

	return nil
}

// Validate validates a document
func (s Schema) Validate(b Block) error {
	return s.validator.Validate(b.Payload)
}

// Digest generates a unique digest for the document if no custom digester is defined
// a SHA256 of the current document payload will be generated
func (s Schema) Digest(b Block) ([]byte, error) {
	return s.digester.Digest(b.Payload)
}

// IndexesFor returns the index kv's for this document
func (s Schema) IndexesFor(b Block) ([]IndexTuple, error) {
	var idxs = make([]IndexTuple, len(s.indexes))

	for i, idx := range s.indexes {
		vals, err := s.valsForResultFields(idx, b.Payload)
		if err != nil {
			return []IndexTuple{}, err
		}

		idxs[i] = IndexTuple{
			Slug:   idx.Name,
			Unique: idx.Unique,
			Vals:   vals,
		}
	}

	return idxs, nil
}

func (s Schema) valsForResultFields(idx Index, payload []byte) ([]string, error) {
	var (
		items gjson.Result
		elems = make([]gjson.Result, len(idx.Fields))
	)

	if idx.Path == "" {
		items = gjson.ParseBytes(payload)
	} else {
		items = gjson.GetBytes(payload, idx.Path)
	}

	if items.IsArray() {
		vals := make([]string, 0, len(items.Array()))

		for _, item := range items.Array() {
			for i, v := range idx.Fields {
				elems[i] = item.Get(v)
			}

			val, err := valForElems(elems, idx.Fields)
			if err != nil {
				return []string{}, err
			}

			if val != "" {
				vals = append(vals, val)
			}
		}

		return vals, nil
	}

	for i, v := range idx.Fields {
		elems[i] = items.Get(v)
	}

	val, err := valForElems(elems, idx.Fields)

	if err != nil {
		return []string{}, err
	}
	if val == "" {
		return []string{}, nil
	}

	return []string{val}, nil
}

func valForElems(elems []gjson.Result, fields []string) (string, error) {
	vals := make([]string, 0, len(elems))

	for _, elm := range elems {
		if elm.IsArray() || elm.IsObject() {
			return "", fmt.Errorf("fields cannot be arrays or objects only scalar values")
		}
		val := elm.String()

		if val != "" {
			vals = append(vals, val)
		}
	}

	if len(vals) == 0 {
		return "", nil
	}

	return strings.Join(vals, "::"), nil
}

// Map maps a document to a specific mapping
func (s Schema) Map(m string, b Block) (map[string]interface{}, error) {
	mapper, ok := s.mappings[m]
	if !ok {
		return nil, nil
	}

	return mapper.Map(b)
}

// UnmarshalJSON implements unmarshal JSON
func (s *Schema) UnmarshalJSON(b []byte) error {
	var (
		indexes   []Index
		validator Validator
	)

	res := gjson.GetManyBytes(b, "version", "validator", "indexes")

	if err := json.Unmarshal([]byte(res[2].String()), &indexes); err != nil {
		return err
	}

	if err := json.Unmarshal([]byte(res[1].String()), &validator); err != nil {
		return err
	}

	s.Version = uint32(res[0].Uint())
	s.validator = validator
	s.indexes = indexes
	s.mappings = map[string]Mapper{}

	return nil
}

// MarshalJSON ...
func (s Schema) MarshalJSON() ([]byte, error) {
	var t = struct {
		Version   uint32    `json:"version"`
		Validator Validator `json:"validator"`
		Indexes   []Index   `json:"indexes"`
	}{
		Version:   s.Version,
		Validator: s.validator,
		Indexes:   s.indexes,
	}

	return json.Marshal(&t)
}
